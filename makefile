CC = g++
BIN = bin
INC = include
LIB = lib
OBJ = obj
SRC = src
EXECUTABLES = test
TEST = test
ROOT = ../..
CFLAGS = -g -Wall -std=c++11 -I$(INC) -I$(ROOT)/$(INC)

HEADERS = include/* $(ROOT)/$(INC)/neuro.h

lib:$(LIB)/libneuro.a

all: $(OBJ)/element.o $(OBJ)/outputElement.o $(OBJ)/delay.o $(OBJ)/mrNeuron.o $(OBJ)/mrSynapse.o $(OBJ)/mrNetwork.o $(OBJ)/test.o $(BIN)/test

$(OBJ)/element.o: $(SRC)/components/element.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/components/element.cpp -c -o $(OBJ)/element.o

$(OBJ)/outputElement.o: $(SRC)/components/outputElement.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/components/outputElement.cpp -c -o $(OBJ)/outputElement.o

$(OBJ)/delay.o: $(SRC)/components/delay.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/components/delay.cpp -c -o $(OBJ)/delay.o

$(OBJ)/mrSynapse.o: $(SRC)/components/mrSynapse.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/components/mrSynapse.cpp -c -o $(OBJ)/mrSynapse.o

$(OBJ)/mrNeuron.o: $(SRC)/components/mrNeuron.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/components/mrNeuron.cpp -c -o $(OBJ)/mrNeuron.o

$(OBJ)/mrNetwork.o: $(SRC)/network/mrNetwork.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/network/mrNetwork.cpp -c -o $(OBJ)/mrNetwork.o

$(OBJ)/mrDannaDevice.o: $(SRC)/network/mrDannaDevice.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/network/mrDannaDevice.cpp -c -o $(OBJ)/mrDannaDevice.o

$(OBJ)/NeuroNetwork.o: $(SRC)/network/NeuroNetwork.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/network/NeuroNetwork.cpp -c -o $(OBJ)/NeuroNetwork.o

$(OBJ)/NeuroDevice.o: $(SRC)/network/NeuroDevice.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(SRC)/network/NeuroDevice.cpp -c -o $(OBJ)/NeuroDevice.o

$(OBJ)/test.o: $(TEST)/test.cpp $(HEADERS)
		$(CC) $(CFLAGS) $(TEST)/test.cpp -c -o $(OBJ)/test.o

$(BIN)/test: $(OBJ)/test.o $(HEADERS) $(OBJ)/element.o $(OBJ)/outputElement.o $(OBJ)/mrNeuron.o $(OBJ)/mrSynapse.o $(OBJ)/delay.o $(OBJ)/mrNetwork.o  
		$(CC) $(CFLAGS) -o $(BIN)/test $(OBJ)/test.o $(OBJ)/mrNetwork.o $(OBJ)/element.o $(OBJ)/mrNeuron.o $(OBJ)/mrSynapse.o $(OBJ)/delay.o $(OBJ)/outputElement.o 

$(LIB)/libneuro.a: $(OBJ)/element.o $(OBJ)/outputElement.o $(OBJ)/delay.o $(OBJ)/mrSynapse.o $(OBJ)/mrNeuron.o $(OBJ)/mrNetwork.o $(OBJ)/mrDannaDevice.o $(OBJ)/NeuroNetwork.o $(OBJ)/NeuroDevice.o
	ar r $(LIB)/libneuro.a $(OBJ)/element.o $(OBJ)/outputElement.o $(OBJ)/delay.o $(OBJ)/mrSynapse.o $(OBJ)/mrNeuron.o $(OBJ)/mrNetwork.o $(OBJ)/mrDannaDevice.o $(OBJ)/NeuroNetwork.o $(OBJ)/NeuroDevice.o
	ranlib $(LIB)/libneuro.a

clean:
	rm -f  $(OBJ)/* $(BIN)/* $(LIB)/* 


#ifndef mrNetwork_CPP
#define mrNetwork_CPP

#include "tools.h"
#include "mrNetwork.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <cstring>
#include <cstdint>
#include <bitset>

using namespace std;

/* Constructor for reading network from input file
 * and reading the input from the network file
 */
mrNetwork::mrNetwork(char *filename, char *inFile, int Max)
{
	string line;
//	int val;
	inCount = 0;
	outCount = 0;
	input = inFile;
	clockcycle = 0;
	clockcycleMax = Max; 
	readNetworkFile(filename);
	setInputFile(inFile);
	rng = NULL;
}



//Destructor
//whats probably happening here is that we delete the neuron
//and simultaneously erase from the map
//how to fix
mrNetwork::~mrNetwork()
{
	//int gridSize = mrNetworkGrid.size();
//	mrNeuron *tempN;
//	mrSynapse *tempS;
//	delay *tempD;

/*	
	cout<<"ACTIVITY INFO" <<endl;  
	cout<<GetActivity() <<endl;
	cout<<"END ACTIVITY INFO" <<endl;
*/	
	vector<double> inCoord;
	vector<double> outCoord;
	inCoord.resize(embeddedDimension);
	outCoord.resize(embeddedDimension);
	for(int i = 0; i < embeddedDimension; i++){
		inCoord[i] = -1.0;
		outCoord[i] = -2.0;
	}


	for(unsigned int i = 0; i < inputComponents.size(); i++){	
		delete inputComponents[i];
			
	}

	if(mrNetworkGrid.size() == 0){
		return;
	}
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		if(it->second->getType() == "I"){
			it->second->dropIncoming(inCoord);
		}
		else if(it->second->getType() == "O"){
			it->second->remove(outCoord);
		}
		//Use destruct neuron so we dont invalidate the iterator
		destructNeuron(it->second);
	}

}

uint32_t mrNetwork::Random_32() {
    if (rng == NULL) {
        fprintf(stderr, "mrNetwork::Random_32 error: rng is NULL.\n");
        #ifdef CONTINUE_ON_ERROR
            return ERROR_VALUE;
        #endif
        exit(1);
    }
    return (*rng)->Random_32();
}

double mrNetwork::Random_Double() {
    if (rng == NULL) {
        fprintf(stderr, "mrNetwork::Random_Double error: rng is NULL.\n");
        #ifdef CONTINUE_ON_ERROR
            return ERROR_VALUE;
        #endif
        exit(1);
    }
    return (*rng)->Random_Double();
}


void mrNetwork::startNetwork()
{
	//string PRINTEVENTS = NeuroUtils::ParamsGetString("mrdanna","PRINTEVENTS",true);
	ifstream inputfile;
	inputfile.open(input);
	if(!inputfile.is_open()){
		MRDANNA_ERROR("Failed to open file");
		exit(1);	
	}

	string line;
//	char i;
//	int cycle, value;
//	int gridSize = mrNetworkGrid.size();
	vector<bool> fireVector;

//	mrNeuron *n;
//	mrSynapse *s;
//	delay *d;

	fireVector.resize(inCount);
    for(clockcycle=0;clockcycle<clockcycleMax;clockcycle++){
   
		 /* At the start of each cycle we clear the output vector
		  * this works logically with the network
		  * but might not be totally true to HW
		  */
		fill(outputVector.begin(),outputVector.end(),false);
		getline(inputfile, line);
		if(!inputfile.eof()){
			setInputVector(line, fireVector);
		}
		else{
			fill(fireVector.begin(),fireVector.end(),false);
		}
		for(unsigned int j = 0; j < inputComponents.size(); j++){
			if(fireVector[j]){
				inputComponents[j]->apply(1.0,clockcycle,NULL);
			}
		}
		for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
			it->second->update(clockcycle);
		}
		
		for(ElementMap::iterator it = delays.begin(); it != delays.end(); ++it){
			it->second->update(clockcycle);
		}
	/*
		if(PRINTEVENTS == "TRUE"){
			vector<string> neuronEvents;
			vector<int> eventTimes;
			for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
				it->second->reportEvents(clockcycle,neuronEvents,eventTimes);
				for(int i = 0; i < (int)neuronEvents.size(); i++){
					events[eventTimes[i]].push_back(neuronEvents[i]);
				}
				neuronEvents.clear();
				eventTimes.clear();

			}
		}
	*/
		trackOutputs();
	}

}

//Later update this to undo LTP/LTD?
void mrNetwork::reset()
{
//	int gridSize = mrNetworkGrid.size();
//	mrNeuron *n;
//	mrSynapse *s;
//	delay *d;
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		it->second->reset();
	}

	for(ElementMap::iterator it = delays.begin(); it != delays.end(); ++it){
		it->second->reset();
	}
}


//Read a network input file

void mrNetwork::readNetworkFile(const char* filename)
{
	//things we need to do in here:
	//catch bad input files

	string txt;
	int dim;
	double lims;

	string line;
	ifstream inputFile(filename);
	getline(inputFile,line);

	istringstream s1;
	s1.str(line);
	s1 >> txt >>dim;
	if(txt != "Embedded:"){
		MRDANNA_ERROR("Expected embedded network");
	}
	embeddedDimension = dim;
	maxDims.resize(dim);
	getline(inputFile, line);
	s1.clear();
	s1.str(line);
	s1 >> txt;
	if(txt != "MaxDims:"){
		MRDANNA_ERROR("Expected max dims param");
		cout<<txt <<endl;
	}
	for(int i = 0; i < dim; i++){
		s1 >> lims;
		maxDims[i] = lims;
	}
	getline(inputFile, line);
	s1.clear();
	s1.str(line);

	s1 >> txt >>dim;
	if(txt != "In:"){
		MRDANNA_ERROR("Expected input neuron count");
	}
	inCount = dim;
	inputComponents.resize(inCount);
	getline(inputFile, line);
	s1.clear();
	s1.str(line);

	s1 >> txt >>dim;
	if(txt != "Out:"){
		MRDANNA_ERROR("Expected output neuron count");
	}
	outCount = dim;

	nIn = inCount;
	nOut = outCount;
	mrNeuron* current;
	mrNeuron* n;
	vector<double> c;
	vector<double> currentN;
	c.resize(embeddedDimension);
	currentN.resize(embeddedDimension);
	while(getline(inputFile, line)){

		istringstream ss(line);
		char blank, first;
		string type;
		int inputWeight;
		int inputDelay;
		ss >> first;
		if(line.empty()){
			first = 'E';
		}
		int val;
		/*
		coordinates currentN;
		coordinates c;
		c.dimension = embeddedDimension;
		currentN.dimension = embeddedDimension;
		c.coords.resize(embeddedDimension);
		currentN.coords.resize(embeddedDimension);
		*/

		if(first == 'S'){
			ss >> inputWeight;
			ss >> blank >>inputDelay;
			mrSynapse *s = new mrSynapse(inputWeight, "S", this);
			for(int i = 0; i < embeddedDimension; i++){
				c[i] = -1.0;
			}
			current->addInputElement((element*) s, c);
			delay *d = new delay(inputDelay,"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
			d->addOutputElement((element*) s);
			s->addInputElement((element*) d);
			s->addOutputElement((element*)current);
			inputComponents[current->IOid]= ((element*)d);
		}
		//case for a syapnse, put in in the neuron's synapse container and its output neuron on the network

		else if(first == 'W'){
			int d;
			int w;
			double position;

			ss >> w >> blank >> d >> type;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				c[i] = position;
			}
			mrSynapse *s = new mrSynapse(w, "W", this);
			//make the delay struct with the neuron and synapse pointers, and delay value
			delay *dy = new delay(d, "D");
		//	cout<<"Getting delay: " <<d <<endl;
			delays.insert(pair<uint64_t,element*>((uint64_t)dy,(element*)dy));
			mapKey++;
			dy->addOutputElement((element*)s);
			s->addInputElement((element*)dy);
			current->addOutputElement((element*)dy, c);
			//get in the input neuron for the synapse
			dy->addInputElement((element*)current);
			//mrNeuron *n;
			//Add a check here later to ensure a valid delay value
		//	n = (mrNeuron *) mrNetworkGrid[c];

			if(mrNetworkGrid.find(c) != mrNetworkGrid.end()){
				n = mrNetworkGrid.find(c)->second;
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
			else{
				n = new mrNeuron(c, type, this);
				mrNetworkGrid.insert(pair< vector<double>, mrNeuron*>(n->coords,n));
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
		}
		else if(first == 'D'){
			int d;
			int w;
			double position;

			ss >> d >> blank >> w >> type;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				c[i] = position;
			}
			mrSynapse *s = new mrSynapse(w, "W", this);
			//make the delay struct with the neuron and synapse pointers, and delay value
			delay *dy = new delay(d, "D");
		//	cout<<"Getting delay: " <<d <<endl;
			delays.insert(pair<uint64_t,element*>((uint64_t)dy,(element*)dy));
			mapKey++;
			dy->addOutputElement((element*)s);
			s->addInputElement((element*)dy);
			current->addOutputElement((element*)dy, c);
			//get in the input neuron for the synapse
			dy->addInputElement((element*)current);
		//	mrNeuron *n;
			//Add a check here later to ensure a valid delay value
			//n = (mrNeuron *) mrNetworkGrid[c];
			if(mrNetworkGrid.find(c) != mrNetworkGrid.end()){
				n = mrNetworkGrid.find(c)->second;
				
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
			else{
				//n = new mrNeuron(c, type);
				n = new mrNeuron(1,type,0,c, this);
				mrNetworkGrid.insert(pair< vector<double>, mrNeuron*>(n->coords,n));
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
		}
		else if(first == 'I' || first == 'O' || first == 'N'){
		//case for a neuron, if the neuron isn't on the network yet, add it. If it is, then get the remainder of the information missing for it
			if(first == 'I' || first == 'O'){
				ss >> val;
			}
			double position;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				currentN[i] = position;
			}
			
			int r;
			float t;
			string blank1, blank2; 
		//	mrNeuron* n;
			if(mrNetworkGrid.find(currentN) == mrNetworkGrid.end()){
				//cout<<"adding a new neuron" <<endl;
				ss >> blank1 >> r >> blank2 >> t;
				string convType(1,first);
				current = new mrNeuron(r, convType, t,currentN, this);
				//mrNetworkGrid[n->coords] = n;
				mrNetworkGrid.insert(pair<vector<double>,mrNeuron*>(current->coords,current));
				if(mrNetworkGrid.find(current->coords) == mrNetworkGrid.end()){
					cout<<"failed to find inserted neuron" <<endl;
				}
			}
			else{
				ss >> blank1 >> r >> blank2 >> t;
				NeuronGrid::iterator it = mrNetworkGrid.find(currentN);
				//mrNetworkGrid[current->coords]->setRefractoryandThreshold(r, t);
				it->second->setRefractoryandThreshold(r,t);
				current = it->second;
			}
			//n = mrNetworkGrid[currentN];

			//if the neuron is an input neuron, keep track of its value. Also create an input synapse for that input neuron
			if(first == 'I'){
				//mrNetworkGrid[currentN]->IOid = val;	
				current->IOid = val;
			}
			else if(first == 'O'){
				current->IOid = val;
				outputElement* o = new outputElement(current->IOid,&outputVector);
				for(int i = 0; i < embeddedDimension; i++){
					c[i] = -2.0;
				}
				current->addOutputElement((element*)o,c);
			}
		}
	}
	outputVector.resize(outCount);
	trackedOutputs.resize(outCount);
	// ADDED BY KATIE
	int MAXTHRESHOLD = NeuroUtils::ParamsGetInt("mrdanna","MAXTHRESHOLD",true);
	int MAXLEVEL = NeuroUtils::ParamsGetInt("mrdanna","MAXLEVEL",true);
	weightMaxMag = MAXLEVEL;
	maxThreshold = MAXTHRESHOLD; 
}


/*
 * The way we're doing this right now is bloaty
 * later try to make a middle-man method 
 * Avoid two copies of this
 */
void mrNetwork::readSerialNetwork(string serialNetwork)
{
	string txt;
	int dim;
	double lims;

	stringstream inputFile;
	inputFile.str(serialNetwork);
	string line;

	getline(inputFile,line);

	istringstream s1;
	s1.str(line);
	s1 >> txt >>dim;
	if(txt != "Embedded:"){
		MRDANNA_ERROR("Expected embedded network");
	}
	embeddedDimension = dim;
	maxDims.resize(dim);
	getline(inputFile, line);
	s1.clear();
	s1.str(line);
	s1 >> txt;
	if(txt == "CONFIGPARAMS"){
		/*if the network has the paras defined then read them
		 * otherwise use the defaults
		 */
	}
	else{
		/*set params from the include file
		 */

	}
	if(txt != "MaxDims:"){
		MRDANNA_ERROR("Expected max dims param");
		cout<<txt <<endl;
	}
	for(int i = 0; i < dim; i++){
		s1 >> lims;
		maxDims[i] = lims;
	}
	getline(inputFile, line);
	s1.clear();
	s1.str(line);

	s1 >> txt >>dim;
	if(txt != "In:"){
		MRDANNA_ERROR("Expected input neuron count");
	}
	inCount = dim;
	inputComponents.resize(inCount);
	getline(inputFile, line);
	s1.clear();
	s1.str(line);

	s1 >> txt >>dim;
	if(txt != "Out:"){
		MRDANNA_ERROR("Expected output neuron count");
	}
	outCount = dim;

	nIn = inCount;
	nOut = outCount;
	mrNeuron* current;
	mrNeuron* n;
	vector<double> c;
	vector<double> currentN;
	c.resize(embeddedDimension);
	currentN.resize(embeddedDimension);
	while(getline(inputFile, line)){

		istringstream ss(line);
		char blank, first;
		string type;
		int inputWeight;
		int inputDelay;
		ss >> first;
		int val;
		if(line.empty()){
			first = 'E';
		}
		/*
		coordinates currentN;
		coordinates c;
		c.dimension = embeddedDimension;
		currentN.dimension = embeddedDimension;
		c.coords.resize(embeddedDimension);
		currentN.coords.resize(embeddedDimension);
		*/

		if(first == 'S'){
			ss >> inputWeight;
			ss >> blank >>inputDelay;
			mrSynapse *s = new mrSynapse(inputWeight, "S", this);
			for(int i = 0; i < embeddedDimension; i++){
				c[i] = -1.0;
			}
			current->addInputElement((element*) s, c);
			delay *d = new delay(inputDelay,"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
			d->addOutputElement((element*) s);
			s->addInputElement((element*) d);
			s->addOutputElement((element*)current);
			inputComponents[current->IOid]= ((element*)d);
		}
		//case for a syapnse, put in in the neuron's synapse container and its output neuron on the network

		else if(first == 'W'){
			int d;
			int w;
			double position;

			ss >> w >> blank >> d >> type;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				c[i] = position;
			}
			mrSynapse *s = new mrSynapse(w, "W", this);
			//make the delay struct with the neuron and synapse pointers, and delay value
			delay *dy = new delay(d, "D");
		//	cout<<"Getting delay: " <<d <<endl;
			delays.insert(pair<uint64_t,element*>((uint64_t)dy,(element*)dy));
			mapKey++;
			dy->addOutputElement((element*)s);
			s->addInputElement((element*)dy);
			current->addOutputElement((element*)dy, c);
			//get in the input neuron for the synapse
			dy->addInputElement((element*)current);
			//mrNeuron *n;
			//Add a check here later to ensure a valid delay value
		//	n = (mrNeuron *) mrNetworkGrid[c];

			if(mrNetworkGrid.find(c) != mrNetworkGrid.end()){
				n = mrNetworkGrid.find(c)->second;
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
			else{
				n = new mrNeuron(c, type, this);
				mrNetworkGrid.insert(pair< vector<double>, mrNeuron*>(n->coords,n));
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
		}
		else if(first == 'D'){
			int d;
			int w;
			double position;

			ss >> d >> blank >> w >> type;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				c[i] = position;
			}
			mrSynapse *s = new mrSynapse(w, "W", this);
			//make the delay struct with the neuron and synapse pointers, and delay value
			delay *dy = new delay(d, "D");
		//	cout<<"Getting delay: " <<d <<endl;
			delays.insert(pair<uint64_t,element*>((uint64_t)dy,(element*)dy));
			mapKey++;
			dy->addOutputElement((element*)s);
			s->addInputElement((element*)dy);
			current->addOutputElement((element*)dy, c);
			//get in the input neuron for the synapse
			dy->addInputElement((element*)current);
		//	mrNeuron *n;
			//Add a check here later to ensure a valid delay value
			//n = (mrNeuron *) mrNetworkGrid[c];
			if(mrNetworkGrid.find(c) != mrNetworkGrid.end()){
				n = mrNetworkGrid.find(c)->second;
				
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
			else{
				//n = new mrNeuron(c, type);
				n = new mrNeuron(1,type,0,c, this);
				mrNetworkGrid.insert(pair< vector<double>, mrNeuron*>(n->coords,n));
				n->addInputElement((element*)s,current->coords);
				s->addOutputElement((element*)n);
			}
		}
		else{
		//case for a neuron, if the neuron isn't on the network yet, add it. If it is, then get the remainder of the information missing for it
			if(first == 'I' || first == 'O'){
				ss >> val;
			}
			double position;
			for(int i = 0; i < embeddedDimension; i++){
				ss >> position;
				currentN[i] = position;
			}
			
			int r;
			float t;
			string blank1, blank2; 
		//	mrNeuron* n;
			if(mrNetworkGrid.find(currentN) == mrNetworkGrid.end()){
				//cout<<"adding a new neuron" <<endl;
				ss >> blank1 >> r >> blank2 >> t;
				string convType(1,first);
				current = new mrNeuron(r, convType, t,currentN, this);
				//mrNetworkGrid[n->coords] = n;
				mrNetworkGrid.insert(pair<vector<double>,mrNeuron*>(current->coords,current));
				if(mrNetworkGrid.find(current->coords) == mrNetworkGrid.end()){
					cout<<"failed to find inserted neuron" <<endl;
				}
			}
			else{
				ss >> blank1 >> r >> blank2 >> t;
				NeuronGrid::iterator it = mrNetworkGrid.find(currentN);
				//mrNetworkGrid[current->coords]->setRefractoryandThreshold(r, t);
				it->second->setRefractoryandThreshold(r,t);
				current = it->second;
			}
			//n = mrNetworkGrid[currentN];

			//if the neuron is an input neuron, keep track of its value. Also create an input synapse for that input neuron
			if(first == 'I'){
				//mrNetworkGrid[currentN]->IOid = val;	
				current->IOid = val;
			}
			else if(first == 'O'){
				current->IOid = val;
				outputElement* o = new outputElement(current->IOid,&outputVector);
				for(int i = 0; i < embeddedDimension; i++){
					c[i] = -2.0;
				}
				current->addOutputElement((element*)o,c);
			}
		}
	}
	outputVector.resize(outCount);
}

void mrNetwork::setInputFile(char* inputFile)
{
	input = inputFile;
}

//Parses input file and returns vector of which neurons to fire
//we probably need to change the format at some point
//the I's are probably redundant 
void mrNetwork::setInputVector(string line, vector<bool>& inputs)
{
	string word;
	int val;
	//int currentClockCycle;

	stringstream parsedLine(line);
	
	parsedLine >> word >> val;
	if(word != "CC"){
		MRDANNA_ERROR("File format invalid");
	}
	//val = currentClockCycle;
	if(val != clockcycle){
		MRDANNA_ERROR("parsed line and current clock cycle dont match");
	}

	fill(inputs.begin(),inputs.end(),false);
	for(int i = 0; i < inCount; i++){
		parsedLine >> word >>val;
		if(val){
			inputs[i] = true;
		}
	}

}

//continue under assumption that outputs will be public
string mrNetwork::Serialize(char flag)
{	
	//int gridSize = mrNetworkGrid.size();
	string serialNetwork;
	//ElementMap* e;

	serialNetwork = "";
	serialNetwork = serialNetwork + getHeader();
	
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it!= mrNetworkGrid.end(); ++it){
		string neuronInfo;
		it->second->print(neuronInfo,flag);
		serialNetwork += neuronInfo;
	}

	return serialNetwork;
}

//Run for a given number of clock cycles
void mrNetwork::Run(int cycles, vector<bool> &fireVector)
{
//	string PRINTEVENTS = NeuroUtils::ParamsGetString("mrdanna","PRINTEVENTS",true);
	//trackedOutputs.resize(nOut);
	for(int i = 0; i < cycles; i++){
		fill(outputVector.begin(),outputVector.end(),false);
		if(i == 0){
			for(unsigned int j = 0; j < inputComponents.size(); j++){
				if(fireVector[j]){
					inputComponents[j]->apply(1.0,clockcycle,NULL);
				}
			}
		}

		for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
			it->second->update(clockcycle);
		}
		for(ElementMap::iterator it = delays.begin(); it != delays.end(); ++it){
			it->second->update(clockcycle);
		}
/*
		if(PRINTEVENTS == "TRUE"){
			vector<string> neuronEvents;
			vector<int> eventTimes;
			for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
				it->second->reportEvents(clockcycle,neuronEvents,eventTimes);
				for(int i = 0; i < (int)neuronEvents.size(); i++){
					events[eventTimes[i]].push_back(neuronEvents[i]);
				}
				neuronEvents.clear();
				eventTimes.clear();

			}
		}
*/
		trackOutputs();
		clockcycle++;
	}
	//clockcycle += cycles;
/*	
	cout<<"ACTIVITY INFO" <<endl;  
	cout<<GetActivity() <<endl;
	cout<<"END ACTIVITY INFO" <<endl;
*/
}

/* Right now we're tracking times with vectors
 * probably want a better data structure soon
 * this will get things going to verify the network
 */
void mrNetwork::trackOutputs()
{	
	for(unsigned int i = 0; i < outputVector.size(); i++){
		if(outputVector[i]){
			trackedOutputs[i].push_back(clockcycle);
		}
	}
}

vector<vector<int> > mrNetwork::getOutputs()
{
	return trackedOutputs;
}

//Crossover function - calls one of the crossover functions based on input
//defaults to RowCol crossover
/*
vector<mrNetwork *> mrNetwork::Crossover(mrNetwork* partner, string crossoverType)
{
	vector<mrNetwork*> children;
	if(crossoverType == "RC"){
		return CrossoverRC(partner);
	}
	else{
		MRDANNA_ERROR("Invalid crossover type");
	}

}
*/

//Crossover Constructor
mrNetwork::mrNetwork(int inC, int outC, vector<double> &maxD)
{
//	cout<<"Called crossover constructor" <<endl;
	int MAXTHRESHOLD = NeuroUtils::ParamsGetInt("mrdanna","MAXTHRESHOLD",true);
	int MAXLEVEL = NeuroUtils::ParamsGetInt("mrdanna","MAXLEVEL",true);
	clockcycle = 0;
	inCount = inC;
	outCount = outC;
	nIn = inC;
	nOut = outC;
	outputVector.resize(outCount);
	maxDims.resize(maxD.size());
	embeddedDimension = maxDims.size();
	for(unsigned int i = 0; i < maxD.size(); i++){
		maxDims[i] = maxD[i];
	}
	inputComponents.resize(inCount);
	trackedOutputs.resize(outCount);
	// ADDED BY KATIE

	weightMaxMag = MAXLEVEL;
	maxThreshold = MAXTHRESHOLD; 

	rng = NULL;
}

mrNetwork::mrNetwork(int inC, int outC, vector<double> &maxD,double connect, int weightm, int maxt, int avgh)
{
	//cout<<"Called crossover constructor2" <<endl;
	clockcycle = 0;
	inCount = inC;
	outCount = outC;
	nIn = inC;
	nOut = outC;
	outputVector.resize(outCount);
	maxDims.resize(maxD.size());
	embeddedDimension = maxDims.size();
	for(unsigned int i = 0; i < maxD.size(); i++){
		maxDims[i] = maxD[i];
	}
	inputComponents.resize(inCount);
	trackedOutputs.resize(outCount);
	connectivity = connect;
	weightMaxMag = weightm;
	maxThreshold = maxt;
	avgHidden = avgh;
	
	rng = NULL;
}

//Get header string to print network
string mrNetwork::getHeader()
{
	string header = "";
	header = header + "Embedded: " + to_string(embeddedDimension) +'\n';
	header = header + "MaxDims: ";
	for(int i = 0; i < embeddedDimension; i++){
		header = header + to_string(maxDims[i]) + " ";
	}
	header = header + '\n';
	
	header = header + "In: " + to_string(inCount) + '\n';
	header = header + "Out: " + to_string(outCount) + '\n';

	return header;
}

//App config constructor - sets some params that are useful for EO
mrNetwork::mrNetwork(int nInputs,int nOutputs,int avgH, double connect = .5, int e = 2, vector<double> maxD = {0,0}, int maxW = 10,  int maxT = 7)
{
	clockcycle = 0;
	cout<<"config construct" <<endl;
	nIn  = nInputs;
	nOut = nOutputs;
	avgHidden = avgH;
	connectivity = connect;
	embeddedDimension = e;
	if(maxD[0] == 0){
		int dim = max(nIn,nOut);
		for(int i =0; i < e; i++){
			maxDims[i] = dim;
		}
	}
	else{
		maxDims.resize(e);
		for(int i = 0; i < e; i++){
			maxDims[i] = maxD[i];
		}
	}
	weightMaxMag = maxW;
	maxThreshold = maxT;
	trackedOutputs.resize(nOut);
	rng = NULL;
}

//Copy constructor
//2 cases - empty network, just want the params
//nonempty network, need to allocate some new objects
mrNetwork::mrNetwork(mrNetwork* copy)
{
	//cout<<"copy construct" <<endl;
	clockcycle = 0;
	if(copy->mrNetworkGrid.size() == 0){
		nIn = copy->nIn;
		nOut = copy->nOut;
		connectivity = copy->connectivity;
		maxThreshold = copy->maxThreshold;
		avgHidden = copy->avgHidden;
		weightMaxMag = copy->weightMaxMag;
		embeddedDimension = copy->embeddedDimension;
		maxDims.resize(embeddedDimension);
		for(int i = 0; i < embeddedDimension; i++){
			maxDims[i] = copy->maxDims[i];
		}
		trackedOutputs.resize(nOut);
		return;
	}
	else{
//		cout<<"Copy nonempty network" <<endl;
		readSerialNetwork(copy->Serialize());
		weightMaxMag = copy->weightMaxMag;
		maxThreshold = copy->maxThreshold;
		avgHidden = copy->avgHidden;
		connectivity = copy->connectivity;
		nIn = copy->nIn;
		nOut = copy->nOut;

	}
	rng = copy->rng;
}

//Config constructor
mrNetwork::mrNetwork(int nInputs, int nOutputs, string conf)
{
	config = conf;
	//cout<<"config construct" <<endl;
	clockcycle = 0;
	int num;
	string txt;
	istringstream ss(conf);

	ss >> txt;
	if(txt == "Default"){
		ss>> num; 
		nIn = nInputs;
		nOut = nOutputs;
		connectivity = .15;
		int DIMENSION = NeuroUtils::ParamsGetInt("mrdanna","DIMENSION",true);
		int MAXTHRESHOLD = NeuroUtils::ParamsGetInt("mrdanna","MAXTHRESHOLD",true);
		int MAXLEVEL = NeuroUtils::ParamsGetInt("mrdanna","MAXLEVEL",true);
		embeddedDimension = DIMENSION;
		int elementDim = (int)(ceil(pow(num,1.0/2)));
		int dim = max(nIn,nOut);
		
		//Need a rule/param for this!
		dim = (double)max(dim,elementDim);
		
		maxDims.resize(embeddedDimension);
		for(int i =0; i < embeddedDimension; i++){
			maxDims[i] = dim;
		}
		//manually setting this for now!
		avgHidden = dim;
		weightMaxMag = MAXLEVEL;
		maxThreshold = MAXTHRESHOLD; 
		trackedOutputs.resize(nOut);
	
	}
	else{
		MRDANNA_ERROR("config not default");
	}
	rng = NULL;
}

mrNetwork::mrNetwork()
{
	clockcycle = 0;
	// ADDED BY KATIE
	int MAXTHRESHOLD = NeuroUtils::ParamsGetInt("mrdanna","MAXTHRESHOLD",true);
	int MAXLEVEL = NeuroUtils::ParamsGetInt("mrdanna","MAXLEVEL",true);
	weightMaxMag = MAXLEVEL;
	maxThreshold = MAXTHRESHOLD; 
	clockcycleMax = 1600;
	rng = NULL;
}

int mrNetwork::inputSize()
{
	return nIn;
}

int mrNetwork::outputSize()
{
	return nOut;
}

string mrNetwork::getConfig()
{
	string Config = "";
	Config = Config + "Default" + " " + to_string(mrNetworkGrid.size()) + " " + to_string(inCount) + " " + to_string(outCount);


	return Config;

}

void mrNetwork::deleteNeuron(mrNeuron* n)
{
	if(mrNetworkGrid.find(n->coords) == mrNetworkGrid.end()){
		MRDANNA_ERROR("Trying to delete neuron not found in network-del");
		return;
	}

	auto incomingConnections = n->getIncoming();
	NeuronGrid::iterator it2;
	element* del;
	
	for(unsigned int  i = 0; i < incomingConnections.size(); i++){
		it2 = mrNetworkGrid.find(incomingConnections[i]);
		if(it2 == mrNetworkGrid.end()){
			MRDANNA_ERROR("Reported incoming connection not found-del");


		}
		else{
			del = it2->second->getDelayUnit(n->coords);	
			if(del != NULL){
				ElementMap::iterator dit = delays.find((uint64_t)del);
				if(dit == delays.end()){
					MRDANNA_ERROR("Delay unit not registered - DN1");
				}
				else{
					delays.erase(dit->first);
					it2->second->remove(n->coords);
				}
			}
		}
	}
	auto outgoingConnections = n->getOutgoing();
	auto delayList = n->getAllDelayUnits();
	for(unsigned int i = 0; i < delayList.size(); i++){
		ElementMap::iterator dit = delays.find((uint64_t)delayList[i]);
		if(dit == delays.end()){
			MRDANNA_ERROR("Delay unit not registered - DN2");
		}
		else{
			delays.erase(dit->first);
		}
	}
	for(unsigned int i = 0; i < outgoingConnections.size(); i++){
		it2 = mrNetworkGrid.find(outgoingConnections[i]);
		if(it2 == mrNetworkGrid.end()){
			MRDANNA_ERROR("Reported outgoing connection not found-del");
		}
		else{
			it2->second->dropIncoming(n->coords);
		}
	}

	//We delete ougoing delay units here
	//must remove them from the map before deletion
	mrNetworkGrid.erase(n->coords);
	delete n;
}


void mrNetwork::destructNeuron(mrNeuron* n)
{
	if(mrNetworkGrid.find(n->coords) == mrNetworkGrid.end()){
		MRDANNA_ERROR("Trying to delete neuron not found in network");
		return;
	}

	auto incomingConnections = n->getIncoming();
	NeuronGrid::iterator it2;
	element* del;
	
	for(unsigned int  i = 0; i < incomingConnections.size(); i++){
		it2 = mrNetworkGrid.find(incomingConnections[i]);
		if(it2 == mrNetworkGrid.end()){
			MRDANNA_ERROR("Reported incoming connection not found");
			for(unsigned int j = 0; j < incomingConnections.size(); j++){
				cout << incomingConnections[i][j] <<" ";
			}
			cout<<endl;
	
			for(NeuronGrid::iterator ip = mrNetworkGrid.begin(); ip != mrNetworkGrid.end(); ++ip){
				cout<< ip->second->printCoords() <<endl;
			}
			exit(1);
		}
		else{
			del = it2->second->getDelayUnit(n->coords);	
			if(del != NULL){
				ElementMap::iterator dit = delays.find((uint64_t)del);
				if(dit == delays.end()){
					MRDANNA_ERROR("Delay unit not registered - DN1");
				}
				else{
					delays.erase(dit->first);
					it2->second->remove(n->coords);
				}
			}
		}
	}
	auto outgoingConnections = n->getOutgoing();
	auto delayList = n->getAllDelayUnits();
	for(unsigned int i = 0; i < delayList.size(); i++){
		ElementMap::iterator dit = delays.find((uint64_t)delayList[i]);
		if(dit == delays.end()){
			MRDANNA_ERROR("Delay unit not registered - DN2");
		}
		else{
			delays.erase(dit->first);
		}
	}
	for(unsigned int i = 0; i < outgoingConnections.size(); i++){
		it2 = mrNetworkGrid.find(outgoingConnections[i]);
		if(it2 == mrNetworkGrid.end()){
			MRDANNA_ERROR("Reported outgoing connection not found");
			for(unsigned int j = 0; j < outgoingConnections.size(); j++){
				cout << outgoingConnections[i][j] <<" ";
			}
			cout<<endl;
	
			for(NeuronGrid::iterator ip = mrNetworkGrid.begin(); ip != mrNetworkGrid.end(); ++ip){
				cout<< ip->second->printCoords() <<endl;
			}
			exit(1);
		}
		else{
			it2->second->dropIncoming(n->coords);
		}
	}

	//We delete ougoing delay units here
	//must remove them from the map before deletion
	delete n;
}



void mrNetwork::deleteConnection(mrNeuron* from, mrNeuron* to)
{
	element* del = from->getDelayUnit(to->coords);
	if(del == NULL){
		cout<<"from here" <<endl;
		return;
	}
	ElementMap::iterator it = delays.find((uint64_t)del);
	if(it == delays.end()){
		MRDANNA_ERROR("Delay unit not registered - DC");
	}
	else{
		delays.erase(it->first);
	}
	from->remove(to->coords);
	to->dropIncoming(from->coords);
}

int mrNetwork::getTime()
{
	return clockcycle;
}

void mrNetwork::setEOParams(mrNetwork* p)
{
	weightMaxMag = p->weightMaxMag;
	maxThreshold = p->maxThreshold;
	connectivity = p->connectivity;
	avgHidden = p->avgHidden;
	nIn = p->nIn;
	nOut = p->nOut;
}

void mrNetwork::clearOldOutputs()
{
	for(unsigned int i = 0; i < trackedOutputs.size(); i++){
		trackedOutputs[i].clear();
	}

}

void mrNetwork::Restore()
{
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		auto synapses = it->second->getSynapses();
		for(unsigned int i = 0; i < synapses.size(); i++){
			synapses[i]->reset();
		}
	}

}

string mrNetwork::GetActivity()
{
	string activity = "";
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		string neuronActivity = "";
		it->second->GetActivity(neuronActivity);
		activity = activity + neuronActivity;
	}

	for(unsigned int i = 0; i < inputComponents.size(); i++){
		string inputAct = "";
		inputComponents[i]->GetActivity(inputAct);
		activity = activity + inputAct;
	}

	return activity;

}

//Deletes unnecessary neurons and synapses
//meant to be used on a pre-built network
bool mrNetwork::pruneNetwork()
{
	vector<mrNeuron*> unused;
	do{
		unused.clear();
		for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
			if(it->second->noIO()){
				unused.push_back(it->second);
			}
		}
		for(unsigned int i = 0; i < unused.size(); i++){
			deleteNeuron(unused[i]);
		}

	}while(!unused.empty());
	return true;
}

void mrNetwork::printNeuronStates()
{
	for(int i = 0; i < clockcycle; i++){
		cout<<"CYCLE " <<i <<endl;
		for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
			cout<< it->second->printCoords() <<" " <<it->second->collectAccums[i];
			if(find(it->second->collectFireds.begin(),it->second->collectFireds.end(),i) != it->second->collectFireds.end()){
				cout<<" Fired" <<endl;
			}
			else{
				cout<<" NOTFired" <<endl;
			}
		}
	}			

}

void mrNetwork::setOutputSize()
{
	trackedOutputs.resize(nOut);
}

string mrNetwork::printEvents()
{
	string s = "";
	for(map<int,vector<string> >::iterator it = events.begin(); it != events.end(); ++it){
		for(int i = 0; i < (int)it->second.size(); i++){
			s+= it->second[i] + '\n';
		}

	}
	return s;
}

int mrNetwork::IsInputIgnored(int inputid)
{
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		if(it->second->IOid == inputid && it->second->getType() == "I"){
			return it->second->IOignored();
		}
	}
	return 0;
}

int mrNetwork::IsOutputIgnored(int outputid)
{
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		if(it->second->IOid == outputid && it->second->getType() == "O"){
			return it->second->IOignored();
		}
	}
	return 0;
}

#endif

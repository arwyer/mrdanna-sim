#ifndef NeuroNetwork_CPP
#define NeuroNetwork_CPP

//get all the includes here
#include "neuro.h"
#include "tools.h"
#include "mrNetwork.h"

string NeuroGetDeviceConfig(string id, int elements, int inputs, int outputs)
{
	string Config = "";
	if(id == "Default"){
		//Default embedding to 2D
		Config = Config + id + " " + to_string(elements) + " " + to_string(inputs) + " " + to_string(outputs);
		return Config;
	}
	else{
		return id;
		string txt;
	
	}
	return "Error in Get Device Config";
}

NeuroNetwork::NeuroNetwork(AppConfig &appconfig)
{

	mrNetwork* m = new mrNetwork(appconfig.nInputs, appconfig.nOutputs, appconfig.deviceConfig);
	model = (void*)m;
	m->rng = &rng;
	rng = NULL;
}

//Crossover losing info here passing networks using serial
NeuroNetwork::NeuroNetwork(const string &fnameOrSerialization, bool isfile)
{
	mrNetwork* m = new mrNetwork();
	if(!isfile){
		m->readSerialNetwork(fnameOrSerialization);		
		model = (void*)m;
	}
	else{
		string s = fnameOrSerialization;
		m->readNetworkFile(s.c_str());
		model = (void*)m;
	}
	m->rng = &rng;
	rng = NULL;
}

NeuroNetwork::NeuroNetwork(const NeuroNetwork &network)
{
	mrNetwork *orig, *copy;
	orig = (mrNetwork*)network.model;
	copy = new mrNetwork(orig);
	copy->setEOParams(orig);
	model = (void*)copy;

	for (auto it = copy->mrNetworkGrid.begin(); it != copy->mrNetworkGrid.end(); it++) {
		it->second->network = copy;
	}
	rng = network.rng;
	copy->rng = &rng;
}

string NeuroNetwork::Serialize()
{
	mrNetwork* m = (mrNetwork*)model;
	return m->Serialize();
}

//This might not be a great way to do this
NeuroNetwork* NeuroNetwork::Copy()
{
	NeuroNetwork* n = new NeuroNetwork(Serialize(),false);
	mrNetwork* m = (mrNetwork*)n->model;
	m->setEOParams((mrNetwork*)model);
	return n;
}

int NeuroNetwork::NInputs()
{
	mrNetwork* m;
	m = (mrNetwork*)model;
	return m->inputSize();
}

int NeuroNetwork::NOutputs()
{
	mrNetwork* m;
	m = (mrNetwork*)model;
	return m->outputSize();
}



string NeuroNetwork::Config()
{
	mrNetwork* m;
	m = (mrNetwork*) model;
	return m->getConfig();
}

NeuroNetwork::~NeuroNetwork()
{
//	cout<<"Using the NeuroNet destructor" <<endl;
	mrNetwork* m = (mrNetwork*)model;
	delete m;
}

bool NeuroNetwork::Prune()
{
	mrNetwork* m = (mrNetwork*)model;
	return m->pruneNetwork();
}

int NeuroNetwork::IsInputIgnored(int inputid)
{
	mrNetwork* m = (mrNetwork*)model;
	return m->IsInputIgnored(inputid);
}

int NeuroNetwork::IsOutputIgnored(int outputid)
{
	mrNetwork* m = (mrNetwork*)model;
	return m->IsOutputIgnored(outputid);
}

#endif

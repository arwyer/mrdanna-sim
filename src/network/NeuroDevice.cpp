#ifndef NeuroDevice_CPP
#define NeuroDevice_CPP

#include "mrDannaDevice.h"
#include "mrNetwork.h"
#include "neuro.h"

NeuroDevice::NeuroDevice(const string &config, const string &device_specific_info)
{
	mrDannaDevice* m;
	m = new mrDannaDevice(config,device_specific_info);
	model = (void*)m;
}

NeuroDevice::~NeuroDevice()
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	delete m;
}

void NeuroDevice::LoadNetwork(NeuroNetwork *network)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->LoadNetwork(network);
}

//Dont worry about this without LTP/LTD enabled
void NeuroDevice::PullCurrentNetwork(NeuroNetwork* network )
{
	/*
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->PullCurrentNetwork(network);
		
	*/
}

void NeuroDevice::ApplyInput(int id, double val, double time)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->ApplyInputs(id, val, time);

}

void NeuroDevice::ApplyInputs(vector<int> &ids, vector<double> &vals, vector<double> &times)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->ApplyInputs(ids,vals,times);

}

void NeuroDevice::Run(double time)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->Run(time);
}

double NeuroDevice::GetTime()
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	return m->GetTime();
}

void NeuroDevice::SetUpOutput(int id, char type, double aftertime)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->SetUpOutput(id,type,aftertime);
}

double NeuroDevice::OutputLastFire(int id)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	return m->OutputLastFire(id);
}

int NeuroDevice::OutputCount(int id)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	return m->OutputCount(id);
}

void NeuroDevice::OutputVector(int id, vector<double> &times, vector<double> &vals)
{
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->OutputVector(id,times,vals);
}

void NeuroDevice::Clear()
{
	//cout<<"clear" <<endl;
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->Clear();
}

void NeuroDevice::ClearActivity()
{
	//cout<<"clear ac" <<endl;
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->ClearActivity();
}

void NeuroDevice::Restore()
{
	//cout<<"restore" <<endl;
	mrDannaDevice* m;
	m = (mrDannaDevice*)model;
	m->Restore();
}

/** SetBatchMode()/GetBatchMode() - Store/Get the batch pointer. **/
void NeuroDevice::SetBatchMode(NeuroBatch *b)
{
  mrDannaDevice *m;
  m = (mrDannaDevice *) model;
  m->batch = b;
}

NeuroBatch *NeuroDevice::GetBatchMode()
{
  mrDannaDevice *m;
  m = (mrDannaDevice *) model;
  return m->batch;
}

#endif

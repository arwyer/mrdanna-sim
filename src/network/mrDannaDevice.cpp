#ifndef mrDannaDevice_CPP
#define mrDannaDevice_CPP

#include "mrNetwork.h"
#include "mrDannaDevice.h" 

mrDannaEvent::mrDannaEvent(int inputId, int inputClockCycle, double val){
	id = inputId;
	clockCycle = inputClockCycle;
	weight = val;
	//cout<<"Making event with val " <<val <<endl;
}

//For use in priority queue - note reverse ordering
bool operator<(mrDannaEvent const &e1, mrDannaEvent const &e2)
{
	return e1.clockCycle > e2.clockCycle;
}

mrDannaDevice::mrDannaDevice(const string &config, const string &device_specific_info)
{
	conf = config;
	istringstream ss(config);
	string type;
	int vals;
	int elements;
	ss >>type >>elements; 
	ss >> vals;
	nInput = vals;
	ss >> vals;
	nOutput = vals;
	
	fireVector.resize(nInput);
	//printf("Filling here\n");
	//cout<<"size: " <<fireVector.size();
	fill(fireVector.begin(),fireVector.end(),false);
	//printf("Finished fill\n");
	outputType.resize(nOutput);
	outputAfterTime.resize(nOutput);
	clockCycle = 0;
	neuroNet = NULL;
	batch = NULL;
}

mrDannaDevice::mrDannaDevice()
{
	
	//fireVector.resize(neuroNet->NInputs());
	//ill(fireVector.begin(),fireVector.end(),false);
	cout<<"Using empty device constructor" <<endl;
	clockCycle = 0; 
	neuroNet = NULL;
	batch = NULL;
}

//Changing this here
//Do we need to delete the old network?
void mrDannaDevice::LoadNetwork(NeuroNetwork* network)
{
	Clear();
	neuroNet = new NeuroNetwork(*network);
	mrNetwork* m = (mrNetwork*)neuroNet->model;
	clockCycle = m->getTime();
}


void mrDannaDevice::PullCurrentNetwork(NeuroNetwork* network)
{

	mrNetwork* m1 = (mrNetwork*)neuroNet->model;
	mrNetwork* m2 = (mrNetwork*)network->model;
	delete m2; 
	m2 = new mrNetwork();
	m2->rng = &network->rng;
	m2->readSerialNetwork(m1->Serialize('F'));
	network->model = (void*)m2;

}

void mrDannaDevice::ApplyInputs(int id, double val, double time)
{
	//cout<<"Emplacing time  " <<time <<" at CC " <<clockCycle  <<endl;
	eventQueue.emplace(id,(int)time + clockCycle,val);

}

void mrDannaDevice::ApplyInputs(vector<int> &ids, vector<double> &vals, vector<double> &times)
{
	if((ids.size() != vals.size()) || (ids.size() != times.size())){
		MRDANNA_ERROR("Input vectors not the same size");
	}
	for(unsigned int i = 0; i < ids.size(); i++){
	//cout<<"Emplacing time  " <<times[i] <<" at CC " <<clockCycle  <<endl;
		eventQueue.emplace(ids[i],(int)times[i] + clockCycle,vals[i]);
	}

}

//Adding a call here to drop outputs from last run
void mrDannaDevice::Run(double time)
{
	lastRun = clockCycle;
	int endTime = (int)time + clockCycle;
	mrNetwork* mn = (mrNetwork*)neuroNet->model;
	mn->clearOldOutputs();
	mn->setOutputSize();

	for(int i = eventQueue.size(); i > 0; i = eventQueue.size()){
		int t = eventQueue.top().clockCycle;
		if(t > endTime){
			//cout<<"Got event at time " <<t <<" breaking " <<endl;
			break;
		}
		else if(t == clockCycle){
			fireVector[eventQueue.top().id] = true;
			eventQueue.pop();
			//cout<<"got event at time " <<t <<" setting " <<endl;
		}
		else if(t > clockCycle){
			mn->Run(t - clockCycle,fireVector);
			fill(fireVector.begin(),fireVector.end(),false);
			clockCycle = t;
			//cout<<"got event at time " <<t <<" running " <<endl;
		}

	}
	if(endTime > clockCycle){
		mn->Run(endTime - clockCycle,fireVector);
		fill(fireVector.begin(),fireVector.end(),false);
		clockCycle = endTime;
		//cout<<"cleaning up" <<endl;
	}
/*	
	cout<<"BEGIN ACTIVITY INFO\n";
	cout<<mn->GetActivity();
	cout<<"END ACTIVITY INFO\n";
*/	
}

//assuming aftertime is the "base" time
//also possible it is "base" with respect to the start of the run
//should probably figure this out
void mrDannaDevice::SetUpOutput(int id, char type, double aftertime)
{
	outputType[id] = type;
	outputAfterTime[id] = aftertime + clockCycle;
}

double mrDannaDevice::OutputLastFire(int id)
{
	if(outputType[id] != 'L'){
		MRDANNA_ERROR("Output was not set to last fire");
	}
	mrNetwork* mn = (mrNetwork*)neuroNet->model;
	auto fireInfo = mn->getOutputs();
	if(!fireInfo[id].empty()){
		int lastFire = fireInfo[id][fireInfo[id].size() -1];
		if(lastFire < (outputAfterTime[id] + lastRun)){
			return -1;
		}
		else{
			return (double)(lastFire-lastRun);
		}
	}
	else{
		return -1;
	}
}

//Need to know if strictly after 
int mrDannaDevice::OutputCount(int id)
{
	if(outputType[id] != 'C'){
		MRDANNA_ERROR("Output was not set to count");
	}
	mrNetwork* mn = (mrNetwork*)neuroNet->model;
	auto fireInfo = mn->getOutputs();
	if(fireInfo[id].empty()){
		return 0;
	}
	int count = 0;
	//cout<<outputAfterTime[id] <<" " <<lastRun <<endl;
	for(vector<int>::reverse_iterator rit = fireInfo[id].rbegin(); rit != fireInfo[id].rend(); ++rit){
		//cout<<"Event: " <<*rit <<" looking after: " <<(outputAfterTime[id] + lastRun) <<endl;
		if(*rit >= outputAfterTime[id] + lastRun){
			//	cout<<" hey im counting" <<endl;
			//cout<< "found event at " <<*rit <<" while looking for events at " <<lastRun <<endl;
			count++;
		}
		else{
			break;
		}

	}
	if(count != 0){
		//cout<<"Count for " <<id <<" is " <<count <<endl;
	}
	
	return count;

}
void mrDannaDevice::OutputVector(int id, vector<double> &times, vector<double> &vals)
{
	if(outputType[id] != 'V'){
		MRDANNA_ERROR("Output was not set to count");
	}
	mrNetwork* mn = (mrNetwork*)neuroNet->model;
	auto fireInfo = mn->getOutputs();
	if(fireInfo[id].empty()){
		//cout<<"empty vec\n";
		return;
	}
	//Charges defaulted to 1 right now
	for(unsigned int i = 0; i < fireInfo[id].size(); i++){
		if(fireInfo[id][i] >= lastRun + outputAfterTime[id]){
			times.push_back((double)fireInfo[id][i] - lastRun);
			vals.push_back(1.0);
		}
	}
}

void mrDannaDevice::Clear()
{
	eventQueue = priority_queue<mrDannaEvent>();
	if(neuroNet != NULL){
		delete neuroNet;
		neuroNet = NULL;
	}
}

//Think we need to delete here
void mrDannaDevice::ClearActivity()
{
	eventQueue = priority_queue<mrDannaEvent>();
	/*
	fireVector.clear();
	outputType.clear();
	outputAfterTime.clear();
	*/
	fill(fireVector.begin(),fireVector.end(),false);
	mrNetwork* m = (mrNetwork*)neuroNet->model;	
	m->clearOldOutputs();
	m->reset();
}

//Need to undo LTP/LTD
//Look at code that uses synapse weight(printing, possibly others);
void mrDannaDevice::Restore()
{
	//LTP/LTD turned off for now
	
	
	mrNetwork* m = (mrNetwork*)neuroNet->model;
	//cout<< m->GetActivity();	
	ClearActivity();
	m->Restore();
	//Function here to undo LTP/LTD
}

mrDannaDevice::~mrDannaDevice()
{
	if(neuroNet != NULL){
		delete neuroNet;
	}
	//cout<<"Using the mrdanadevice destructor" <<endl;

}

double mrDannaDevice::GetTime()
{
	return (double)clockCycle;

}

#endif

#ifndef delay_CPP
#define delay_CPP

//namespace and includes
#include "delay.h"


delay::delay(int delay,string t)
{
	d1[0] = 0;
	d1[1] = 0;
	buf[0] = 0;
	buf[1] = 0;
	ccDelay = delay;
	std::uint64_t holder = 1;
	if(delay >=64){
		d1[1] = d1[1] | (holder << delay); // get a 64 bit value, with only one 1,
		//and shift that 1 over as many times left as the value is,                       //then store that as the delay(while ORing it with 64 0's)
	}
	else{
		d1[0] = d1[0] | (holder << delay);
	}
	type = t;
	charges.resize(ccDelay);
	if(ccDelay == 0){
		charges.resize(1);
	}
	fill(charges.begin(),charges.end(),0.0);
	head = 0;
	chargesPassed = 0;
}


void delay::delayChange(int i){ 

	ccDelay = i;
	std::uint64_t holder = 1;

	if(i >=64){
		d1[1] = d1[1] | (holder << i);
	}
	else{
		d1[0] = d1[0] | (holder << i);
	}
}


//adding a charge onto the buf for firing

/*
void delay::apply(float w, int clockcycle){ 
	// add another charge onto the buffer of charges
	if(ccDelay >= 64){ 
		buf[1] = buf[1] | d1[1];
	}
	else{
		buf[0] = buf[0] | d1[0];
	}
	return;
}
*/

void delay::apply(float w, int clockcycle, element* sender){ 
	// add another charge onto the buffer of charges
	chargesPassed++;
	if(head != 0){
		charges[head - 1] = w;
	}
	else{
		charges[charges.size()-1] = w;
	}
}
/*
void delay::update(int clockCycle){
	std::uint64_t check = 1;
	if((buf[0] ^ check) == (buf[0]-1)){ // if the xor of buf[0] and 1 equals 1 less than buf[0] we know
		//that buf[0] had a 1 in the LSB place so a charge needs to be passed to the synapse
	//	cout<<"Delay to synapse at " <<clockCycle <<" with delay " <<ccDelay <<endl;	
		for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
			it->second->apply(0,clockCycle);
		}
	}

	buf[0] = buf[0] >> 1;
	buf[0] |= (buf[1] << 63);
	buf[1] = buf[1] >> 1;

	return;

}
*/

void delay::update(int clockCycle){

	if(charges[head] != 0.0){
		for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
			it->second->apply(charges[head],clockCycle,this);
		}
	}
	charges[head] = 0.0;
	if(head != (charges.size() - 1)){
		head++;
	}
	else{
		head = 0;
	}
	

}

void delay::addInputElement(element* e)
{
	inputElements.insert(pair<int, element*>(mapKey, e));
	mapKey++;
}

void delay::addOutputElement(element* e)
{
	outputElements.insert(pair<int, element*>(mapKey, e));
	mapKey++;
}

void delay::print(string &s, char flag){
	printRecursive(s, ccDelay);
	return;
}

void delay::reset()
{
	std::uint64_t holder = 1;
	if(ccDelay >=64){
		d1[1] = d1[1] | (holder << ccDelay);
	}
	else{
		d1[0] = d1[0] | (holder << ccDelay);
	}
	buf[0] = 0;
	buf[1] = 0;
	fill(charges.begin(),charges.end(),0.0);

}

void delay::feedback(int f)
{
	MRDANNA_ERROR("feedback not implemented for delay");
}

void delay::GetActivity(string &s)
{
	s += "Delay: Cycles " + to_string(ccDelay) + " Charges " + to_string(chargesPassed) + '\n';
	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		string outgoing = "";
		it->second->GetActivity(outgoing);
		s = s + outgoing;
	}
}


#endif

#ifndef mrSynapse_CPP
#define mrSynapse_CPP

//namespace
#include "mrSynapse.h"



void mrSynapse::changeWeight(int clockcycle){ // will determine if LTP/LTD is required, need to figure out how to mathematically determine what to adjust by
	//function is not currently being used
	if(clockcycle == currentClockCycle){
		if((clockcycle-1) == lastClockCycle){

		}
		else{ // will depress the synapse
			weight = weight - 1;//weightConstant;
		}
	}
	else if((clockcycle-1) == currentClockCycle){//will pontentiate the synapse
		weight = weight + 1;//weightConstant;
	}
	return;
}


mrSynapse::mrSynapse(int w, string t, mrNetwork* pnetwork)
{
	weight = w;
	eWeight = w;
	lastClockCycle = 0;
	currentClockCycle = 0;
	weightConstant = .3;
	mapKey = 0;
	type = t;
	initWeight = w;
	numLTD = 0;
	numLTP = 0;
	numActive = 0;
	network = pnetwork;
	
	/*Set constant hardware values here
	 *Move this to read from file eventually
	 *But these shouldnt change too much so this is fine for now
	 */
	double MEMHRS = NeuroUtils::ParamsGetDouble("mrdanna","MEMHRS",true);
	double MEMLRS = NeuroUtils::ParamsGetDouble("mrdanna","MEMLRS",true);
	int MAXLEVEL = NeuroUtils::ParamsGetInt("mrdanna","MAXLEVEL",true);

	max = MAXLEVEL; //The maximum granularity of the weights
	
	HRS = MEMHRS; //The maximum resistance of the memristor
	LRS = MEMLRS;  //The minimum resistance of the memristor
	GeffMax = (1.0/MEMLRS) - (1.0/MEMHRS); //Maximum ideal conductance
	normG = GeffMax/(1.0*max); //Normalize conductance to weight
	float a = -normG*weight;
	float b = 2.0 + (normG*weight*(HRS+LRS));
	float c = -(HRS+LRS);
	Rp = (-b + sqrt(b*b - 4.0*a*c))/(2.0*a);
	//Rn = 1.0/(1.0/(Rp - (Geff*weight)));
	Rn = 1.0/((1.0/Rp) - (normG*weight));
	Geff = (1.0/Rp) - (1.0/Rn); //this should be geffmax/weight i think
	
	//delR = 4.09E3;
	double CLOCKPERIOD = NeuroUtils::ParamsGetDouble("mrdanna","CLOCKPERIOD",true);
	double POSITIVESWITCHTIME = NeuroUtils::ParamsGetDouble("mrdanna","POSITIVESWITCHTIME",true);
	double NEGATIVESWITCHTIME = NeuroUtils::ParamsGetDouble("mrdanna","NEGATIVESWITCHTIME",true);
	double POSITIVETHRESHOLDVOLTAGE = NeuroUtils::ParamsGetDouble("mrdanna","POSITIVETHRESHOLDVOLTAGE",true);
	double NEGATIVETHRESHOLDVOLTAGE = NeuroUtils::ParamsGetDouble("mrdanna","NEGATIVETHRESHOLDVOLTAGE",true);
	double LEARNINGVOLTAGE = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGVOLTAGE",true);

	pdelR = (HRS-LRS)*(CLOCKPERIOD/POSITIVESWITCHTIME)*(LEARNINGVOLTAGE/POSITIVETHRESHOLDVOLTAGE);
	ndelR = (HRS-LRS)*(CLOCKPERIOD/NEGATIVESWITCHTIME)*(LEARNINGVOLTAGE/NEGATIVETHRESHOLDVOLTAGE);
	//So we need to separate this into positive and negative delR
	//so the switching will be assymetric
	//maybe as a first step check to see if it works with existing networks?

	//Assuming that we can ignore wf and delR init for now
	/* Initialize internal values here*/

}

//These are null for now, reserving for power profiling
//Add method to reset to initial weight 
void mrSynapse::reset()
{
	weight = initWeight;
	eWeight = initWeight;

	/* Reset internal values here*/
	float a = -normG*weight;
	float b = 2.0 + (normG*weight*(HRS+LRS));
	float c = -(HRS+LRS);
	Rp = (-b + sqrt(b*b - 4.0*a*c))/(2.0*a);
	Rn = 1.0/(1.0/(Rp - (Geff*weight)));
	Geff = (1.0/Rp) - (1.0/Rn);
}

void mrSynapse::update(int clockCycle)
{
}

//if synapse gets any sort of impulse it is set to fire
//this behavior might change later
void mrSynapse::apply(float w, int clockCycle, element* sender)
{
	numActive++;
	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		it->second->apply(weight*w,clockCycle,this);
	}
}

void mrSynapse::addInputElement(element* e)
{
	inputElements.insert(pair<int, element*>(mapKey, e));
	mapKey++;
}

void mrSynapse::addOutputElement(element* e)
{
	outputElements.insert(pair<int, element*>(mapKey, e));
	mapKey++;
}

//Printing out initweight from here - come back here if errors start to happen!
void mrSynapse::print(string &s, char flag)
{
	if(flag == 'I'){
		printRecursive(s, initWeight);
	}
	else if(flag == 'F'){
		printRecursive(s, (int)weight);
	}
	return; 
}

int mrSynapse::getWeight()
{
	return weight;
}

void mrSynapse::randWeight(int weightMaxMag)
{
	
	weight = (network->Random_32()%weightMaxMag) + 1;
	if(network->Random_32()%2){
		weight = weight*-1;
	}

	eWeight = weight;
	initWeight = weight;
	
	/*Set internals here*/
	float a = -normG*weight;
	float b = 2.0 + (normG*weight*(HRS+LRS));
	float c = -(HRS+LRS);
	Rp = (-b + sqrt(b*b - 4.0*a*c))/(2.0*a);
	Rn = 1.0/(1.0/(Rp - (Geff*weight)));
	Geff = (1.0/Rp) - (1.0/Rn);
}

void mrSynapse::feedback(int potOrDep,int val)
{
	string LEARNING = NeuroUtils::ParamsGetString("mrdanna","LEARNING",true);
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	if(LEARNING == "OFF"){
		return;
	}
	if(type == "S"){
		return;
	}
	if(potOrDep == 1){
		if(LEARNINGTYPE == "Real"){
			potentiateReal();
		}
		else if(LEARNINGTYPE == "Constant"){
			potentiateConstant();
		}
		else if(LEARNINGTYPE == "STDP"){
				potentiateSTDP(val);
		}
		numLTP++;
	}
	if(potOrDep == -1){
		if(LEARNINGTYPE == "Real"){
			depressReal();
		}
		else if(LEARNINGTYPE == "Constant"){
			depressConstant();
		}
		else if(LEARNINGTYPE == "STDP"){
				depressSTDP(val);
		}
		numLTD++;
	}
}

/*Simple LTP and LTD for now
 * values gotten from VLSI paper
 * be prepared to change behavior here
 * possible changes-
 * nonlinear switching
 * switching of different magnitude
 * assymetric switching
 */
void mrSynapse::potentiateConstant()
{
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	weight = weight + LEARNINGPARAM;
	eWeight = eWeight + LEARNINGPARAM;

	if(weight > max){
		weight = max;
		eWeight = max;
	}
}

void mrSynapse::depressConstant()
{
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	weight = weight - LEARNINGPARAM;
	eWeight = eWeight - LEARNINGPARAM;
	
	if(weight < (-1.0*max)){
		weight = -1*max;
		eWeight = -1*max;
	}

}

void mrSynapse::GetActivity(string &s)
{
	s += "Synapse: Cycles_Active " + to_string(numActive) + " Potentiations " + to_string(numLTP) + " Depressions " + to_string(numLTD) + '\n';
	return;

}

/* Potentiation based on how the device actually does it*/
void mrSynapse::potentiateReal()
{
	//Based off assumption positive is HRS -> LRS, negative LRS -> HRS
	Rn += ndelR;
	Rp -= pdelR;
	Geff = (1.0/Rp) - (1.0/Rn);
	weight = Geff/normG;
	if(weight > max){
		weight = max;
		Geff = GeffMax;
		Rn = HRS;
		Rp = LRS;
	}
	eWeight = weight;
	
}

/*Depressin based on how the device will do it*/
void mrSynapse::depressReal()
{

	Rn -= pdelR;
	Rp += ndelR;
	Geff = (1.0/Rp) - (1.0/Rn);
	weight = Geff/normG;
	if(weight < (-1.0*max)){
		Geff = -1.0*GeffMax;
		Rn = LRS;
		Rp = HRS;
	}
	eWeight = weight;

}

void mrSynapse::potentiateSTDP(int val)
{
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	Rn += (ndelR*(LEARNINGPARAM-val + 1));
	Rp -= (pdelR*(LEARNINGPARAM-val + 1));
//	Rn += (delR/val);
//	Rp -= (delR/val);
	Geff = (1.0/Rp) - (1.0/Rn);
	weight = Geff/normG;
	if(weight > max){
		weight = max;
		Geff = GeffMax;
		Rn = HRS;
		Rp = LRS;
	}
	eWeight = weight;
	
}

/*Depressin based on how the device will do it*/
void mrSynapse::depressSTDP(int val)
{
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);

	Rn -= (pdelR*(LEARNINGPARAM-val+1));
	Rp += (ndelR*(LEARNINGPARAM-val+1));
//	Rn -= (delR/val);
//	Rp += (delR/val);
	Geff = (1.0/Rp) - (1.0/Rn);
	weight = Geff/normG;
	if(weight < (-1.0*max)){
		Geff = -1.0*GeffMax;
		Rn = LRS;
		Rp = HRS;
	}
	eWeight = weight;

}

#endif

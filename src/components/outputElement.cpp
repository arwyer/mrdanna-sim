#ifndef outputElement_CPP
#define outputElement_CPP

#include "outputElement.h"

void outputElement::apply(float w, int clockCycle, element* sender)
{
	(*outputVector)[position] = true;
	//cout<<"Firing at " <<clockCycle <<endl;
}

outputElement::outputElement(int pos, vector<bool>* v)
{
	position = pos;
	epos = pos;
	outputVector = v;
	type = "OE";
}

void outputElement::print(string &s,char flag)
{
	s = "output";
	return;
}

void outputElement::GetActivity(string &s)
{
	s = "";
	return;
}

void outputElement::update(int clockCycle)
{
	MRDANNA_ERROR("OE update");

}

void outputElement::feedback(int f)
{
	MRDANNA_ERROR("feedack not implemented for output element");
}

#endif

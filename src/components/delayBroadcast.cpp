#ifndef delayBroadcast_CPP
#define delayBroadcast_CPP

delayBroadcast::delayBroadcast(int delay){
	d1[0] = 0;
	d1[1] = 0;
	buf[0] = 0;
	buf[1] = 0;
	ccDelay = delay;
	std::uint64_t holder = 1;
	if(val >=64){
		d1[1] = d1[1] | (holder << val); // get a 64 bit value, with only one 1,
		//and shift that 1 over as many times left as the value is,                       //then store that as the delay(while ORing it with 64 0's)
	}
	else{
		d1[0] = d1[0] | (holder << val);
	}

}
													
void delayBroadcast::delayChange(int i){

	ccDelay = i;
	std::uint64_t holder = 1;

	if(i >=64){
		d1[1] = d1[1] | (holder << i);
	}
	else{
		d1[0] = d1[0] | (holder << i);
	}
}

//adding a charge onto the buf for firing
void delay::apply(float w, int clockcycle, element* sender){ 
	// add another charge onto the buffer of charges

	if(ccDelay >= 64){
		buf[1] = buf[1] | d1[1];
	}
	else{
		buf[0] = buf[0] | d1[0];
	}
	return;
}

void delay::update(int clockcycle){

	std::uint64_t check = 1;
	if((buf[0] ^ check) == (buf[0]-1)){ // if the xor of buf[0] and 1 equals 1 less than buf[0] we know
		//that buf[0] had a 1 in the LSB place so a charge needs to be passed to the synapse
		//
		for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
			it->second->apply(0,clockCycle,this);
		}
	}

	buf[0] = buf[0] >> 1;
	buf[0] |= (buf[1] << 63);
	buf[1] = buf[1] >> 1;

	return;

}


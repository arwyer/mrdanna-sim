#ifndef element_CPP
#define element_CPP

#include "element.h"

void element::apply(float w, int clockCycle, element* sender)
{
	MRDANNA_ERROR("Element apply");
	//not implemented for element
}

void element::reset()
{

	//not implemented for element yet
}

void element::print(string &s,char flag)
{
	MRDANNA_ERROR("Print Not Implemented for generic element");	
}

void element::printRecursive(string &s, int param, char flag)
{
	//cout<<"Element " <<getType() << " printing";
	string empty = "";
	s = s + " " + type + " " + to_string(param);
	for(ElementMap::iterator it = outputElements.begin(); it!= outputElements.end(); ++it){
	//	cout<<" next is : " <<it->second->getType() <<endl;
		if(it->second->getType() == "N" || it->second->getType() == "I" || it->second->getType() == "O"){
			s = s + " " + it->second->getType() + " ";
	//		cout <<"String: " <<s <<endl;
			return ;
		}
		else if(it->second->type == "OE"){
			s = "output";
			return;
		}
		else{
			it->second->print(s);
			return;
		}
	}
}

string element::getType()
{
	return type;
}

int element::getId()
{
	MRDANNA_ERROR("Should only be called by neuron");
	return -1;
}

void element::update(int clockCycle)
{
	MRDANNA_ERROR("Element update");
}

element::~element(){
	/*recursively call destructor 
	 * down to the next neuron
	 * neurons will override this
	 * call destructor for all of their connections
	 * traverse backwards through inputs and call delete at the end
	 */
	deleteConnections();
	
}

void element::deleteConnections()
{

	/*recursively call destructor 
	 * down to the next neuron
	 * neurons will override this
	 * call destructor for all of their connections
	 * traverse backwards through inputs and call delete at the end
	 */

	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		if(it->second->getType() == "N" || it->second->getType() == "I" || it->second->getType() == "O"){
			return;
		}
		else if(it->second->getType() == "OE"){
//			MRDANNA_ERROR("Deleting output element from generic destructor");
			delete it->second;
		}
		else{
			delete it->second;
		}
	}

}

int element::getConnectionWeight()
{
	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		if(it->second->getType() == "W"){
			return it->second->eWeight;
		}
		else{
			return it->second->getConnectionWeight();
		}

	}
	return -1;
}

element* element::getDelayUnit()
{
	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		if(it->second->getType() == "D"){
			return it->second;
		}
		else{
			return it->second->getDelayUnit();
		}

	}
	//If no delay unit to return
	//Check if this causes issues elsewhere with fix
	return NULL;
}
	


element* element::findSynapse()
{
	for(ElementMap::iterator it = outputElements.begin(); it != outputElements.end(); ++it){
		if(it->second->getType() == "W"){
			return it->second;
		}
		else if(it->second->getType() == "OE"){
			return NULL;
		}
		else{
			return it->second->findSynapse();
		}

	}
	return NULL;	

}

void element::feedback(int type, int val)
{
	MRDANNA_ERROR("feedback not implemented for element");
}

void element::GetActivity(string&s)
{
	MRDANNA_ERROR("not implemented for element");
}


#endif

#ifndef mrNeuron_CPP
#define mrNeuron_CPP

//namespace
#include "mrNeuron.h"

//Taking x and y directly now, move to vector for generic dimensions
mrNeuron::mrNeuron(int r, string ty, int t, vector<double> &c, mrNetwork* pnetwork){
	network = pnetwork;
	refractoryPeriod = r;
	type = ty;
	threshold = t;
	accumulator = 0.0;
	inRefracPeriod = false;
	fireNextCycle = false;
	coords.resize(c.size());
	coords.assign(c.begin(),c.end());
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalCycles = 0;
	totalAccum = 0;
	gotInput = false;
	signals.resize(1);
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	if(LEARNINGTYPE == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}
}

mrNeuron::mrNeuron(vector<double> &c,string ty, mrNetwork* pnetwork){
	network = pnetwork;
	type = ty;
	accumulator = 0.0;
	inRefracPeriod = false;
	fireNextCycle = false;

	coords.resize(c.size());
	coords.assign(c.begin(),c.end());
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalCycles = 0;
	totalAccum = 0;
	gotInput = false;
	signals.resize(1);
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	if(LEARNINGTYPE == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}
	/*
	for(int i = 0; c.size(); i++){
		coords[i] = 0.0;
		coords[i] = c[i];
	}
	*/

	/*
	coords.dimension = c.coords.size(); 
	coords.coords.resize(c.coords.size());
	coords.coords.assign(c.coords.begin(),c.coords.end());
	*/
}

/*Random Neuron constructor
 * note: defaulting refrac to 1
 * might want to change this later
 */
mrNeuron::mrNeuron(vector<double>& maxDims, int maxThresh, string ty, mrNetwork* pnetwork)
{
	network = pnetwork;
	string REFRACTYPE = NeuroUtils::ParamsGetString("mrdanna","REFRACTYPE",true);
	int MAXREFRACPERIOD = NeuroUtils::ParamsGetInt("mrdanna","MAXREFRACPERIOD",true);
	int MINREFRACPERIOD = NeuroUtils::ParamsGetInt("mrdanna","MINREFRACPERIOD",true);
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);

	inRefracPeriod = false;
	if(REFRACTYPE == "FIXED"){
		refractoryPeriod = MAXREFRACPERIOD;
	}
	else if(REFRACTYPE == "VAR"){
		refractoryPeriod = network->Random_32()%(MAXREFRACPERIOD - MINREFRACPERIOD + 1) + MINREFRACPERIOD;
	}
	threshold = network->Random_32()%maxThresh + 1;
	accumulator = 0.0;
	fireNextCycle = false;
	cycleLastFired = -1;
	type = ty;
	coords.resize(maxDims.size());

	for(unsigned int i = 0; i < maxDims.size(); i++){
//		coords[i] = (network->Random_32() / ( RAND_MAX / (maxDims[i] - 0.0)));
		coords[i] = network->Random_32()%(int)maxDims[i];
	}
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalCycles = 0;
	totalAccum = 0;
	gotInput = false;
	signals.resize(1);
	if(LEARNINGTYPE == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}
}


void mrNeuron::setRefractoryandThreshold(int r, int t){
	refractoryPeriod = r;
	threshold = t;
}

bool mrNeuron::inRefPeriod(){
	return inRefracPeriod;
}

void mrNeuron::reset()
{
	//Make sure this is everything
	accumulator = 0.0;
	inRefracPeriod = false;
	fireNextCycle = false;
	for(unsigned int i = 0; i < signals.size(); i++){
		signals[i].clear();
	}
	
}


//Attempting to rewrite this
//Want to clean up the logic a bit
//and add support for STDP
void mrNeuron::apply(float w, int clockCycle, element* sender)
{
	double THRESHOLDLIMIT = NeuroUtils::ParamsGetDouble("mrdanna","THRESHOLDLIMIT",true);
	bool PRINTEVENTS = NeuroUtils::ParamsGetBool("mrdanna","PRINTEVENTS",true);
	bool PRINTEVENTS2 = (NeuroUtils::ParamsGetBool("demo","VIZ_ACTIVE",false) == 1);
	if(PRINTEVENTS || PRINTEVENTS2) {
		string e;
		for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
			if(it->second == sender){
				if(it->first[0] == -1.0){
					e = "Time: " + to_string(clockCycle) + " Type: Pulse Neuron: " + printCoords();
					break;
				}
				else{
					e =  "Time: " + to_string(clockCycle) + " Type: AddCharge Synapse: " + printCoords(it->first) + " --> " + printCoords();
					break;
				}
			}
		}
		cout << e <<endl;
		/*
		events.push_back(e);
		eventTimes.push_back(clockCycle);
		*/
	}
	if(storedCycle != clockCycle){
		update(clockCycle);
	}
	if(inRefracPeriod){
		depressSynapse(sender,(clockCycle-cycleLastFired + 1));
		return;
	}
	if(signals[signals.size()-1].empty()){
		accumInput++;
	}
	signals[signals.size()-1].push_back(sender);
	accumulator += w;
	//Set a lower limit for negative - wont exist for positive
	if(accumulator < (-1.*THRESHOLDLIMIT)){
		accumulator = (-1.0*THRESHOLDLIMIT);
	}
	if(accumulator < threshold){
		fireNextCycle = false;
	}
	else if(accumulator >= threshold){
		fireNextCycle = true;
	}
	
}


void mrNeuron::update(int clockCycle)
{
	//collectAccums.push_back(accumulator);
	
	bool PRINTEVENTS = NeuroUtils::ParamsGetBool("mrdanna","PRINTEVENTS",true);
	bool PRINTEVENTS2 = (NeuroUtils::ParamsGetBool("demo","VIZ_ACTIVE",false) == 1);
	if(storedCycle == clockCycle){
		return;
	}
	totalCycles++;
	storedCycle = clockCycle;
	if(fireNextCycle){
		for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
			it->second->apply(1.0,clockCycle,this);
		}
	if(PRINTEVENTS || PRINTEVENTS2){
			string e;
			e = "Time: " + to_string(clockCycle) + " Type: Fire Neuron: "+ printCoords();
			cout<< e <<endl;
		//	events.push_back(e);
		//	eventTimes.push_back(clockCycle);
		}
		inRefracPeriod = true;
		fireNextCycle = false;
		cycleLastFired = clockCycle;
		accumulator = 0.0;
		for(unsigned int i = 0; i < signals.size(); i++){
			for(unsigned int j = 0; j < signals[i].size(); j++){
				potentiateSynapse(signals[i][j],signals.size()-i);
			}
		}

		for(unsigned int i = 0; i < signals.size() - 1; i++){
			signals[i] = signals[i + 1];
		}
		signals[signals.size() - 1].clear();
		fired++;
		//collectFireds.push_back(clockCycle);
	}
	else{
		for(unsigned int i = 0; i < signals.size() - 1; i++){
			signals[i] = signals[i + 1];
		}
		totalAccum += accumulator;
		if(inRefracPeriod){
			if(cycleLastFired + refractoryPeriod == clockCycle){
				inRefracPeriod = false;
			}
		}
	}
}
/*
void mrNeuron::apply(float w, int clockCycle, element* sender)
{
	if((cycleLastFired + refractoryPeriod) == clockCycle){
		inRefracPeriod = false;
	}
	if(inRefracPeriod){
		depressSynapse(sender);
		return;
	}

	else{
		if(storedCycle != clockCycle){
			if(!fireNextCycle){
				storedCycle = clockCycle;
				signalsReceived.clear();
				signalsReceived.push_back(sender);
				accumInput++;
			}
			else{
				depressSynapse(sender);
				return;
			}
		}
		if(fireNextCycle){
			if(clockCycle == cycleToFire){
				depressSynapse(sender);
				return;
			}
			accumulator += w;
			signalsReceived.push_back(sender);
			if(accumulator < (-1.0*threshold)){
				accumulator = (-1.0*threshold);
			}
			if(accumulator < threshold){
				fireNextCycle = false;
				return;
			}
		}
		else{
			accumulator += w;
			if(signalsReceived.empty()){
				accumInput++;
			}
			signalsReceived.push_back(sender);
			if(accumulator < (-1.0*threshold)){
				accumulator = (-1.0*threshold);
			}
			if(accumulator > threshold){
				fireNextCycle = true;
				cycleToFire = storedCycle + 1;
			}
			return;
		}
	}

}
*/

//Right now we dump full acumulated weight
//Other strategy is adjusted accumulation
//Charge = accumulator/thresh
/*
void mrNeuron::update(int clockCycle)
{
	//float normAccum = accumulator/threshold;
	totalCycles++;
	totalAccum += accumulator;
	if(fireNextCycle){
		if(clockCycle == cycleToFire){
			for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
				it->second->apply(1.0,clockCycle,this);
			}
			inRefracPeriod = true;
			fireNextCycle = false;
			cycleLastFired = clockCycle;
			accumulator = 0.0;
			for(unsigned int i = 0; i < signalsReceived.size(); i++){
				potentiateSynapse(signalsReceived[i]);
			}
			signalsReceived.clear();
			storedCycle = clockCycle;
			fired++;
		}
	}
	else{
		if(storedCycle != clockCycle){
			signalsReceived.clear();
			storedCycle = clockCycle;
		}
		if(inRefracPeriod){
			//update refrac information - check how hardware does this
			if((cycleLastFired + refractoryPeriod) == clockCycle){
				inRefracPeriod = false;
			}
			
		}
	}
	
}
*/

//Need delete functions, probably only the random case
//add for EO when necessary
void mrNeuron::addInputElement(element* e,vector<double> &c)
{
	
	vector<double> v;
	v.resize(c.size());
	v.assign(c.begin(),c.end());

	if(inElements.find(v) != inElements.end()){
		MRDANNA_ERROR("inserting duplicate input element");
		cout<<"trying: ";
		for(unsigned int i = 0; i < c.size(); i++){
			cout<<c[i] <<" ";
		}
		cout<<endl;
		for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
			cout<<printCoords(it->first) <<endl;
		}
		exit(1);
	}
	else{
		inElements.insert(pair< vector<double>, element*>(v, e));
	}
}

void mrNeuron::addOutputElement(element* e,vector<double> &c)
{
	/*
	vector<double> v;
	v.resize(c.size());
	for(int i = 0; i < v.size(); i++){
		v[i] = c[i];
	}
	*/
	vector<double> v;
	v.resize(c.size());
	v.assign(c.begin(),c.end());

	if(outElements.find(v) != outElements.end()){
		MRDANNA_ERROR("inserting duplicate output element");
		cout<<"Inserting connection from " <<printCoords() <<" to " <<printCoords(c) <<endl;
	}
	else{
		/*	
		cout<<"Inserting connection to " <<printCoords(c) << "from " <<printCoords() <<endl;
		for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
			cout<<printCoords(it->first) <<endl;
		}
		*/
		outElements.insert(pair< vector<double>, element*>(v, e));
	}
}

void mrNeuron::print(string &s, char flag)
{
	/* 
	 * Heres how the neurons do the printing
	 * print their neuron info
	 * then go through their list and print for each output component
	 * 
	 */

	if(type == "O" || type == "I"){
		s = s + type + " " + to_string(IOid)+ " "  + printCoords() +  " Refrac: " + to_string(refractoryPeriod) + " Thres: " + to_string(threshold) + '\n';
		if(type == "I"){
			s = s + " S " + to_string(threshold + 1) + " D 0" + '\n';
		}
	}
	else{
		s = s + type + " " + printCoords() + " Refrac: " + to_string(refractoryPeriod) + " Thres: " + to_string(threshold) + '\n';
	}

	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		string connections = "";
		it->second->print(connections);
		//cout<<neuronString <<endl;
		//neuronString += connections;
		//cout<<neuronString <<endl;
		//cout<< it->second->getType() <<endl;
		if(connections != "output"){
			connections += " ";
			connections += printCoords(it->first);
			connections += '\n';
			//neuronString = neuronString + printCoords(it->first);
			//neuronString = neuronString + '\n';
			s += connections;
		}
	}
	//return neuronString;
}



void mrNeuron::deleteRandomConnection()
{
	int randVal = network->Random_32()%outElements.size();
	NeuroIOMap::iterator randConnection = outElements.begin();
	advance(randConnection,randVal);
	if(randConnection->first[0] >= 0.0){
		delete outElements[randConnection->first];
		outElements.erase(randConnection->first);
	}
}

vector<double> mrNeuron::getRandomConnection()
{
	if(outElements.size() != 0){
		int randVal = network->Random_32()%outElements.size();
		NeuroIOMap::iterator randConnection = outElements.begin();
		advance(randConnection, randVal);
		vector<double> con;
		con.resize(randConnection->first.size());
		con.assign(randConnection->first.begin(),randConnection->first.end());
		return con;
	}
	else{
		vector<double> noCon;
		noCon.resize(coords.size());
		noCon[0] = -3.0;
		//MRDANNA_ERROR("NO OUTPUTS");
		return noCon;
	}

}

int mrNeuron::getThreshold()
{
	return threshold;
}

bool mrNeuron::isConnection(vector<double> &to)
{
	if(outElements.find(to) == outElements.end()){
		return false;
	}
	else{
		return true;
	}
}

double mrNeuron::distanceTo(mrNeuron* dest)
{
	if(coords.size() != dest->coords.size()){
		MRDANNA_ERROR("Neuron dimension mismatch");
		//cout<< "My size: " <<coords.size() <<" other size: " <<dest->coords.size() <<endl;
		exit(1);
	}
	double dist = 0;
	for(unsigned int i = 0; i < coords.size(); i++){
		dist += (coords[i] - dest->coords[i])*(coords[i] - dest->coords[i]);
	}
	dist = sqrt(dist);
	return dist;
}

vector< vector<double> > mrNeuron::getOutgoing()
{
	vector<vector<double> > outgoing;
	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		if(it->first[0] >= 0.0){
			outgoing.push_back(it->first);
		}
		else{
			//cout<<"not providing outgoing for " <<printCoords(it->first) <<endl;
		}
	}

	
	return outgoing;
}

vector<vector <double> > mrNeuron::getIncoming()
{
	vector<vector<double> > incoming;
	for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
		if(it->first[0] >= 0.0){
			incoming.push_back(it->first);
		}
		else{
			//cout<<"not providing incoming for " <<printCoords(it->first) <<endl;
		}
	}

	
	return incoming;
}

vector<int> mrNeuron::getIncomingWeights()
{
	
	vector<int> weights;
	for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
		if(it->first[0] >= 0.0){
			weights.push_back(it->second->getConnectionWeight());
		}
	}
	return weights;
}
vector<int> mrNeuron::getOutgoingWeights()
{
	vector<int> weights;
	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		if(it->first[0] >= 0.0){
			weights.push_back(it->second->getConnectionWeight());
		}
	}
	return weights;
}

int mrNeuron::getOutputPos()
{
	if(type != "O"){
		return -1;
	}
	return IOid;
}

int mrNeuron::getInputPos()
{
	if(type != "I"){
		return -1;
	}
	return IOid;
}


//Random constructor for IO neurons
mrNeuron::mrNeuron(vector<double>& maxDims, int maxThresh, string ty, int pos, mrNetwork* pnetwork)
{
	network = pnetwork;
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	int MAXREFRACPERIOD = NeuroUtils::ParamsGetInt("mrdanna","MAXREFRACPERIOD",true);
	inRefracPeriod = false;
	refractoryPeriod = MAXREFRACPERIOD;
	//refractoryPeriod = IOREFRAC;
	threshold = network->Random_32()%maxThresh + 1;
	accumulator = 0.0;
	fireNextCycle = false;
	cycleLastFired = -1;
	type = ty;
	coords.resize(maxDims.size());
	IOid = pos;

	//For now we place the I/O neurons along the first dimension
	//We also assume that 1stdim >= max(I,O)
	
	coords[0] = (double)pos;
	if(type == "I"){
		coords[1] = 0.0;
		for(unsigned int i = 2; i < coords.size(); i++){
			coords[i] = 0.0;
		}
	}
	else if(type == "O"){
		coords[1] = maxDims[1];
		for(unsigned int i = 2; i < coords.size(); i++){
			coords[i] = maxDims[i];
		}
	}
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalCycles = 0;
	totalAccum = 0;
	gotInput = false;
	signals.resize(1);
	if(string(LEARNINGTYPE) == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}
}

//Implemented with "easy" spacing - start at 0 and shuffle down
//consider spacing with enforced centering

mrNeuron::mrNeuron(vector<double>& maxDims, int maxThresh, string ty, int pos,int numTot, mrNetwork* pnetwork)
{
	network = pnetwork;
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
	double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	int MAXREFRACPERIOD = NeuroUtils::ParamsGetInt("mrdanna","MAXREFRACPERIOD",true);
	inRefracPeriod = false;
	refractoryPeriod = MAXREFRACPERIOD;
	threshold = network->Random_32()%maxThresh + 1;
	accumulator = 0.0;
	fireNextCycle = false;
	cycleLastFired = -1;
	type = ty;
	coords.resize(maxDims.size());
	IOid = pos;
	signals.resize(1);
	if(string(LEARNINGTYPE) == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}

	//For now we place the I/O neurons along the first dimension
	//We also assume that 1stdim >= max(I,O)


	//We want to evenly space neurons over the max dimensions
	//not assuming dim >= max(I,O)
	
	double spacing = maxDims[0]/numTot;
	coords[0] = (double)pos*spacing;
	if(type == "I"){
		coords[1] = 0.0;
		for(unsigned int i = 2; i < coords.size(); i++){
			coords[i] = 0.0;
		}
	}
	else if(type == "O"){
		coords[1] = maxDims[1];
		for(unsigned int i = 2; i < coords.size(); i++){
			coords[i] = maxDims[i];
		}
	}
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalCycles = 0;
	totalAccum = 0;
	gotInput = false;
}

void mrNeuron::shuffle(vector<double> maxDims )
{
	/*
	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		delete it->second;
	}
	*/
	for(unsigned int i = 0; i < maxDims.size(); i++){
		coords[i] = network->Random_32()%(int)maxDims[i];
		//coords[i] = (network->Random_32() / ( RAND_MAX / (maxDims[i] - 0.0)));
	
	}
}

//Remove connection to, should rename this
void mrNeuron::remove(vector<double> &oldN)
{
	NeuroIOMap::iterator it = outElements.find(oldN);
	if(it == outElements.end()){
		cout<<"Deleting nonexistent connection" <<endl;
	}
	else{
		int erased;
		delete it->second;
		erased = outElements.erase(it->first);
		if(erased == 0){
			cout<<"Did not erase connection" <<endl;
		}
	}
}

void mrNeuron::randThresh(int maxThreshold)
{
	string THRESHOLDTYPE = NeuroUtils::ParamsGetString("mrdanna","THRESHOLDTYPE",true);
	if(THRESHOLDTYPE == "VAR"){
		threshold = 1 + network->Random_32()%(maxThreshold);
	}
	else if(THRESHOLDTYPE == "FIXED"){
		threshold = maxThreshold;
	}
	else{
		MRDANNA_ERROR("Did not recognize THRESHOLDTYPE");
	}
}

vector<element*> mrNeuron::getSynapses()
{
	vector<element*> synapses;
	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		if(it->first[0] >= 0.0){
			element* e = it->second->findSynapse();
			if(e != NULL){
				synapses.push_back(e);
			}
		}
	}
	return synapses;
}

string mrNeuron::printCoords()
{
	string coordStr = "";
	for(unsigned int i = 0; i < coords.size(); i++){
		coordStr = coordStr + to_string(coords[i]) + " ";
	}
	return coordStr;
}

string mrNeuron::printCoords(const vector<double> &c )
{
	string coordStr = "";
	for(unsigned int i = 0; i < c.size(); i++){
		coordStr = coordStr + to_string(c[i]) + " ";
	}
	return coordStr;
}

/*
mrNeuron::mrNeuron(mrNeuron* copy)
{
	coords.dimension = copy->coords.dimension;
	for(int i = 0; i < coords.dimension; i++){
		coords.coords[i] = copy->coords.coords[i];
	}
	type = copy->type;
	refractoryPeriod = copy->refractoryPeriod;
	threshold = copy->threshold;
	network = copy->network;
}
*/

mrNeuron::~mrNeuron()
{
	/* network takes care of removing you from neighbors
	 * delete all outgoing connections then destruct
	 * neighbors will be notified to remove you
	 */

	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		
		delete it->second;
	}

}

void mrNeuron::dropIncoming(vector<double> &incN)
{
	NeuroIOMap::iterator it = inElements.find(incN);
	if(it == inElements.end()){
		MRDANNA_ERROR("Neuron to be dropped not found");
		cout<<"Neuron: " <<printCoords() <<endl;
		cout<<"Coords to find: ";
		for(unsigned int i = 0; i < incN.size(); i++){
			cout<<incN[i] <<" ";
		}
		cout<<endl;
		for(NeuroIOMap::iterator it2 = inElements.begin(); it2 != inElements.end(); ++it2){
			for(unsigned int i = 0; i < it2->first.size(); i++){
				cout<<it2->first[i] <<" ";
			}
			cout<<endl;
		}
		exit(1);

	
	}
	else{
		inElements.erase(incN);
	}
}

element* mrNeuron::getDelayUnit(vector<double> &c)
{
	NeuroIOMap::iterator it;
	it = outElements.find(c);
	if(it == outElements.end()){
		MRDANNA_ERROR("Delay connection not found");
		
		return NULL;
	}

	if(it->second->getType() == "D"){
		return it->second;
	}
	else{
		return it->second->getDelayUnit();
	}
}

vector<element*> mrNeuron::getAllDelayUnits()
{
	vector<element*> delays;
	for(NeuroIOMap::iterator it = outElements.begin(); it !=outElements.end(); ++it){
		if(it->first[0] >= 0.0){
			if(it->second->getType() == "D"){
				delays.push_back(it->second);
			}
			else{
				delays.push_back(it->second->getDelayUnit());
			}
		}
	}
	return delays;
}

void mrNeuron::depressSynapse(element* S,int val)
{
	for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
		if(it->second == S){
			it->second->feedback(-1,val);
		}
	}

}

void mrNeuron::potentiateSynapse(element* S, int val)
{
	for(NeuroIOMap::iterator it = inElements.begin(); it != inElements.end(); ++it){
		if(it->second == S){
			it->second->feedback(1,val);
		}
	}

}

void mrNeuron::feedback(int f)
{
	MRDANNA_ERROR("feedback not implemented for neuron");
}

void mrNeuron::GetActivity(string &s)
{
	noInput = totalCycles - accumInput - fired;
	s = "Neuron: No_Input " + to_string(noInput) + " Accumulated " + to_string(accumInput) + " Fired " + to_string(fired) + " Total_cycles " + to_string(totalCycles) + " Total_Accumulation " +to_string(totalAccum) + '\n';
	
	for(NeuroIOMap::iterator it = outElements.begin(); it != outElements.end(); ++it){
		string connectionAct = "";
		it->second->GetActivity(connectionAct);
		s = s + connectionAct;
	}

}

bool mrNeuron::noIO()
{
	if(type =="I" || type == "O"){
		return false;
	}
	else if(inElements.empty() || outElements.empty()){
		return true;
	}
	return false;
}

void mrNeuron::reportEvents(int clockCycle,vector<string>& reported, vector<int>& reportTimes)
{
	reported.assign(events.begin(),events.end());
	reportTimes.assign(eventTimes.begin(),eventTimes.end());
	events.clear();
	eventTimes.clear();
	return;
}

int mrNeuron::IOignored()
{
	if(type == "I"){
		if(outElements.empty()){
			return 1;
		}
	}
	else if(type == "O"){
		if(inElements.empty()){
			return 1;
		}
	}
	return 0;

}

#endif

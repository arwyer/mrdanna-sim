import sys

actFile = open(sys.argv[1],"r")
outFile = open(sys.argv[2],"w")
s = ""

neuronList = []
synapseList = []
delayList = []
total = 0
for line in actFile:
	if "Neuron:" in line:
		elems = line.split()
		stats = []
		stats.append(int(elems[2]))
		stats.append(int(elems[4]))
		stats.append(int(elems[6]))
		stats.append(int(elems[8]))
		stats.append(float(elems[10]))
		total = int(elems[8])
		neuronList.append(stats)
	if "Delay:" in line:
		elems = line.split()
		stats = []
		stats.append(int(elems[2]))
		stats.append(int(elems[4]))
		delayList.append(stats)
	if "Synapse:" in line:
		elems = line.split()
		stats = []
		stats.append(int(elems[2]))
		stats.append(int(elems[4]))
		stats.append(int(elems[6]))
		synapseList.append(stats)
count = 0
s += "Neuron, Fires, Accum, Idle, Total\n"
for nstats in neuronList:
	s += str(count) + "," + str(nstats[2]) + "," + str(nstats[1]) + "," + str(total - nstats[1] - nstats[2]) + "," +  str(total) + "\n"
	count += 1
s += ", \n"
count = 0

s+= "Synapse, Active, Passive, Potentiations, Depresions\n"
for sstats in synapseList:
	s+=	str(count) + "," +str(sstats[0]) + "," + str(total - sstats[0]) + "," + str(sstats[1]) + "," +str(sstats[2]) + "\n"
	count += 1

s+= " ,\n"
count = 0
s+= "Delay, Charges, Length\n"
for dstats in delayList:
	s+=	str(count) +"," + str(dstats[1]) + "," + str(dstats[0]) + "\n"
	count += 1
outFile.write(s)

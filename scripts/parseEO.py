import sys
import os
import re


def parseNetwork(f):
	#Function that parses networks
	return 0

def parseDFile(f):
	#Function that parses the output of a Danna EO run
	numNeurons = 0
	numSynapses = 0
	currentEpoch = -1;
	currentFitness = -1;
	for line in f:
		if "Epoch" in line:
			elems = line.split()
			currentEpoch = int(elems[1])
			currentFitness = float(elems[3])
		elif "# CONFIG" in line:
			numNeurons = 0
			numSynapses = 0
		else:
			elems = line.split()
			if len(elems) != 0:
				if elems[0] == "N":
					numNeurons += 1
				elif elems[0] == "S":
					numSynapses += 1	
	
	#print currentEpoch
	#print currentFitness
	#print numNeurons
	#print numSynapses
	params = []
	params.append(currentEpoch)
	params.append(currentFitness)
	params.append(numNeurons)
	params.append(numSynapses)
	return params


def parseMFile(f):
	#Function that parses the output of an EO run
	numNeurons = 0
	numSynapses = 0
	for line in f:
		if "Epoch" in line:
			elems = line.split()
			currentEpoch = int(elems[1])
			currentFitness = float(elems[3])
		elif "Embedded" in line:
			numNeurons = 0
			numSynapses = 0
		elif "Refrac:" in line:
			numNeurons += 1
		else:
			elems = line.split()
			if len(elems) >= 4:
				if elems[0] == "S" and elems[2] == "D":
					numSynapses += 1
				elif elems[0] == "D" and elems[2] == "W":
					numSynapses += 1	
	
	#print currentEpoch
	#print currentFitness
	#print numNeurons
	#print numSynapses
	params = []
	params.append(currentEpoch)
	params.append(currentFitness)
	params.append(numNeurons)
	params.append(numSynapses)
	return params

def usage():
	#specify usage
	print 'Usage: directory_with_files output.csv path_to_eo_defaults.txt M/D/N'

if len(sys.argv) != 5:
	usage()
	sys.exit(1)

if (sys.argv[4] != "M") and (sys.argv[4] != "D") and (sys.argv[4] != "N"):
	print 'Expected M for mrdanna, D for danna, or N for Nida'
	print 'Usage: directory_with_files output.csv path_to_eo_defaults.txt M/D/N'
	sys.exit(1)

outputFile = open(sys.argv[2],'w')
outputFile.write('FileName, StopEpoch, StopFitness, Complete, NumNeurons, NumSynapses \n')


with open(sys.argv[3],'r') as defaultsFile:
	for line in defaultsFile:
		if "stop_epoch" in line:
			stopEpoch = int(re.search('\d+',line).group())
		elif "stop_fitness" in line:
			stopFitness = int(re.search('\d+',line).group())
		elif "fitness_multiplier" in line:
			fitMult = int(re.search('\d+',line).group())

maxFit = stopFitness*fitMult

if os.path.isdir(sys.argv[1]):
	dirAbs = os.path.abspath(sys.argv[1])
	for file in os.listdir(sys.argv[1]):
		with open(sys.argv[1] + '/' + file,'r') as f:
			if sys.argv[4] == "M":
				params = parseMFile(f)
			elif sys.argv[4] == "D":
				params = parseDFile(f)
			elif sys.argv[4] == "N":
				params = parseNFile(f)
			if params[1] == maxFit:
				fin = "Y"
			else:
				fin = "N"

			infoToPrint = file + ", " + repr(params[0]) + ", " + repr(params[1]) + ", " + fin + ", " + repr(params[2]) + ", " + repr(params[3]) + '\n'
						
			outputFile.write(infoToPrint)	

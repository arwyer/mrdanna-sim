import sys
import os
import re


def extract(f,maxFit):
	#Reads an EO run file and extracts best network if run finished
	numNeurons = 0
	numSynapses = 0
	finished = False
	network = 'incomplete'
	for line in f:
		if finished:
			network = network + line
			if "Epoch" in line:
				elems = line.split()
				currentFitness = float(elems[3])
				if currentFitness == maxFit:
					network = ''
		elif "Epoch" in line:
			elems = line.split()
			currentFitness = float(elems[3])
			if currentFitness == maxFit:
				network = ''
				finished = True
	return network

def usage():
	#specify usage
	print 'Usage: directory_with_EO_runs directory_to_put_nets maxFitness'

if len(sys.argv) != 4:
	usage()
	sys.exit(1)



maxFit = int(sys.argv[3])
if not os.path.exists(sys.argv[2]):
	os.makedirs(sys.argv[2])
elif not os.path.isdir(sys.argv[2]):
	print '2nd argument exists but is not directory, Exiting'
	sys.exit(1)

count = 0
if os.path.isdir(sys.argv[1]):
	dirAbs = os.path.abspath(sys.argv[1])
	for file in os.listdir(sys.argv[1]):
		outputfile = open(sys.argv[2] + '/net' + str(count) + '.txt','w')
		with open(sys.argv[1] + '/' + file,'r') as f:
			network = extract(f,maxFit)
			if(network != 'incomplete'):
				outputfile.write(network)
				count += 1
		outputfile.close()

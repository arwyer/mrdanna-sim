#ifndef mrDannaEO_CPP
#define mrDannaEO_CPP

#include "tools.h"
#include "mrNeuron.h"
#include "mrNetwork.h"
#include "neuro.h"



mrNeuron::mrNeuron(mrNeuron* copy)
{
	coords.resize(copy->coords.size());
	for(unsigned int i = 0; i < coords.size(); i++){
		coords[i] = copy->coords[i];
	}
	type = copy->type;
	refractoryPeriod = copy->refractoryPeriod;
	threshold = copy->threshold;
	inRefracPeriod = false;
	accumulator = 0.0;
	fireNextCycle = false;
	cycleLastFired = -1;
	noInput = 0;
	accumInput = 0;
	fired = 0;
	totalAccum = 0;
	totalCycles = 0;
	signals.resize(1);
	string LEARNINGTYPE = NeuroUtils::ParamsGetString("mrdanna","LEARNINGTYPE",true);
    double LEARNINGPARAM = NeuroUtils::ParamsGetDouble("mrdanna","LEARNINGPARAM",true);
	if(LEARNINGTYPE == "STDP"){
		signals.resize((int)LEARNINGPARAM);
	}
}


void NeuroNetwork::Random(string &config)
{
	mrNetwork* m;
	m = (mrNetwork*) model;
	m->Random();

}

void NeuroNetwork::Mutate(string &config)
{
	mrNetwork* m;
	m = (mrNetwork*) model;
	m->mutate();
}

void NeuroNetwork::Crossover(NeuroNetwork* Partner, NeuroNetwork* child1, NeuroNetwork* child2)
{
	mrNetwork* p;
	mrNetwork* m;
	mrNetwork* c1;
	mrNetwork* c2;
	m = (mrNetwork*)model;
	p = (mrNetwork*)Partner->model;

	c1 = (mrNetwork*) child1->model;
	c2 = (mrNetwork*) child2->model;
	m->Crossover(p, c1, c2, "RC");

	// r[0]->rng = rng;
	// c1->rng = &r[0]->rng;

	// r[1]->rng = rng;
	// c2->rng = &r[1]->rng;
	
	c1->setEOParams(m);
	c2->setEOParams(p);
}

void mrNetwork::CrossoverRC(mrNetwork* partner, mrNetwork* child1, mrNetwork* child2)
{
	
	//cout<<"***DOING CROSSOVER***" <<endl;
	//Add check for network dimensions

	// mrNetwork* child1 = new mrNetwork(inCount,outCount,maxDims, connectivity,weightMaxMag,maxThreshold,avgHidden);
	// mrNetwork* child2 = new mrNetwork(inCount,outCount,maxDims, connectivity,weightMaxMag,maxThreshold,avgHidden);

	//mrNetwork* child2 = new mrNetwork(inCount,outCount,maxDims);
	//HERE - set the EO parameters for the children

	child1->clockcycle = 0;
	child1->inCount = inCount;
	child1->outCount = outCount;
	child1->nIn = inCount;
	child1->nOut = outCount;
	child1->inputComponents.resize(inCount);
	child1->trackedOutputs.resize(outCount);
	child1->outputVector.resize(outCount);

	child2->clockcycle = 0;
	child2->inCount = inCount;
	child2->outCount = outCount;
	child2->nIn = inCount;
	child2->nOut = outCount;
	child2->inputComponents.resize(inCount);
	child2->trackedOutputs.resize(outCount);
	child2->outputVector.resize(outCount);

	/* Quick pseudo 
	 * Add to neurogrids of new nets
	 * add fixed structs and neurons from p1 and p2
	 * get list of connections for each neuron
	 * resolve all of the connections
	 * do again for opposite sides of cut
	 */

	map< vector<double>,vector< vector <double> > > edgeContainer1;
	map< vector<double>, vector< vector <double> > > edgeContainer2;
	map< vector<double> ,vector<int> > weightContainer1;
	map< vector<double>, vector<int> > weightContainer2;
	
	//If 0 we cut vertical 1 horizontal
	int RowOrCol = Random_32()%2;
	double cut;
	if(RowOrCol){
		cut = Random_Double()*maxDims[0];
	}
	else{
		cut = Random_Double()*maxDims[1];
	}

	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it!= mrNetworkGrid.end(); ++it){
		if(it->second->getType() == "O"){
			child1->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child1->mrNetworkGrid[it->second->coords]->network = child1;
			int pos = it->second->getOutputPos();
			edgeContainer1[it->second->coords] = it->second->getOutgoing();
			weightContainer1[it->second->coords] = it->second->getOutgoingWeights();
			//vector<bool> *oV =outputVector;
			outputElement* oe = new outputElement(pos,&child1->outputVector);
			vector<double> outC;
			outC.resize(embeddedDimension);
			for(int i = 0; i < embeddedDimension; i++){
				outC[i] = -2.0;
			}
			child1->mrNetworkGrid[it->second->coords]->addOutputElement((element*)oe,outC);
			child1->mrNetworkGrid[it->second->coords]->IOid = pos; 
		}
		else if(it->second->getType() == "I"){
		
			child1->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child1->mrNetworkGrid[it->second->coords]->network = child1;
			edgeContainer1[it->second->coords] = it->second->getOutgoing();
			weightContainer1[it->second->coords] = it->second->getOutgoingWeights();
			int pos = it->second->getInputPos();
			child1->mrNetworkGrid[it->second->coords]->IOid = pos; 
			mrSynapse* is = new mrSynapse(it->second->getThreshold() + 1,"S", this);
			delay* idy = new delay(0,"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)idy,(element*)idy));
			idy->addOutputElement((element*)is);
			is->addInputElement((element*)idy);
			is->addOutputElement((element*)child1->mrNetworkGrid[it->second->coords]);
			child1->inputComponents[pos] = (element*)idy;
			vector<double> inC;
			inC.resize(embeddedDimension);
			for(int i = 0; i < embeddedDimension; i++){
				inC[i] = -1.0;
			}
			child1->mrNetworkGrid[it->second->coords]->addInputElement((element*)is,inC);	

		}
		else if(it->second->coords[RowOrCol] < cut){
			child1->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child1->mrNetworkGrid[it->second->coords]->network = child1;
			edgeContainer1[it->second->coords] = it->second->getOutgoing();
			weightContainer1[it->second->coords] = it->second->getOutgoingWeights();
		}
		else{
			child2->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child2->mrNetworkGrid[it->second->coords]->network = child2;
			edgeContainer2[it->second->coords] = it->second->getOutgoing();
			weightContainer2[it->second->coords] = it->second->getOutgoingWeights();
		}
	}

	for(NeuronGrid::iterator it = partner->mrNetworkGrid.begin(); it!= partner->mrNetworkGrid.end(); ++it){
		if(it->second->getType() == "O"){
			child2->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child2->mrNetworkGrid[it->second->coords]->network = child2;
			edgeContainer2[it->second->coords] = it->second->getOutgoing();
			weightContainer2[it->second->coords] = it->second->getOutgoingWeights();
			int pos = it->second->getOutputPos();
			outputElement* oe = new outputElement(pos,&child2->outputVector);
			vector<double> outC;
			outC.resize(embeddedDimension);
			for(int i = 0; i < embeddedDimension; i++){
				outC[i] = -2.0;
			}
			child2->mrNetworkGrid[it->second->coords]->addOutputElement((element*)oe,outC);
			child2->mrNetworkGrid[it->second->coords]->IOid = pos; 
		}
		else if(it->second->getType() == "I"){
			child2->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child2->mrNetworkGrid[it->second->coords]->network = child2;
			edgeContainer2[it->second->coords] = it->second->getOutgoing();
			weightContainer2[it->second->coords] = it->second->getOutgoingWeights();
			int pos = it->second->getInputPos();
			child2->mrNetworkGrid[it->second->coords]->IOid = pos; 
			mrSynapse* is = new mrSynapse(it->second->getThreshold() + 1,"S", this);
			delay* idy = new delay(0,"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)idy,(element*)idy));
			idy->addOutputElement((element*)is);
			is->addInputElement((element*)idy);
			is->addOutputElement((element*)child2->mrNetworkGrid[it->second->coords]);
			child2->inputComponents[pos] = (element*)idy;
			vector<double> inC;
			inC.resize(embeddedDimension);
			for(int i = 0; i < embeddedDimension; i++){
				inC[i] = -1.0;
			}
			child2->mrNetworkGrid[it->second->coords]->addInputElement((element*)is,inC);	

		}
		else if(it->second->coords[RowOrCol] > cut){
			child1->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child1->mrNetworkGrid[it->second->coords]->network = child1;
			edgeContainer1[it->second->coords] = it->second->getOutgoing();
			weightContainer1[it->second->coords] = it->second->getOutgoingWeights();
		}
		else{
			child2->mrNetworkGrid[it->second->coords] = new mrNeuron(it->second);
			child2->mrNetworkGrid[it->second->coords]->network = child2;
			edgeContainer2[it->second->coords] = it->second->getOutgoing();
			weightContainer2[it->second->coords] = it->second->getOutgoingWeights();
		}
	}

	mrSynapse* s;
	delay* d;

	for(NeuronGrid::iterator it = child1->mrNetworkGrid.begin(); it != child1->mrNetworkGrid.end(); ++it){
		for(unsigned int i = 0; i < edgeContainer1[it->first].size(); i++){
			if(child1->mrNetworkGrid.find(edgeContainer1[it->first][i]) == child1->mrNetworkGrid.end()){
				mrNeuron* closest;
				double dist = (maxDims[0]*maxDims[0]) + (maxDims[1]*maxDims[1]);
				for(NeuronGrid::iterator it2 = child1->mrNetworkGrid.begin(); it2 != child1->mrNetworkGrid.end(); ++it2){
					if(it != it2){
						if(dist > it->second->distanceTo(it2->second)){
							closest = it2->second;
							dist = it->second->distanceTo(it2->second);
						}
					}
				}
				if(!it->second->isConnection(closest->coords)){
					d = new delay((int)round(dist),"D");
					delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					s = new mrSynapse(weightContainer1[it->first][i],"W", this);
					d->addOutputElement((element*)s);
					d->addInputElement((element*)it->second);
					s->addInputElement((element*)d);
					s->addOutputElement((element*)closest);
					child1->delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					it->second->addOutputElement((element*)d,closest->coords);
					closest->addInputElement((element*)s,it->second->coords);
				}
			}
			else{
				NeuronGrid::iterator match = child1->mrNetworkGrid.find(edgeContainer1[it->first][i]);
				if(!it->second->isConnection(match->second->coords)){
					double dist = it->second->distanceTo(match->second);	
					d = new delay((int)round(dist),"D");
					delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					s = new mrSynapse(weightContainer1[it->first][i],"W", this);
					d->addOutputElement((element*)s);
					d->addInputElement((element*)it->second);
					s->addInputElement((element*)d);
					s->addOutputElement((element*)match->second);
					child1->delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					it->second->addOutputElement((element*)d,match->second->coords);
					match->second->addInputElement((element*)s,it->second->coords);
				}
			}
		}
	}
			
	for(NeuronGrid::iterator it = child2->mrNetworkGrid.begin(); it != child2->mrNetworkGrid.end(); ++it){
		for(unsigned int i = 0; i < edgeContainer2[it->first].size(); i++){
			if(child2->mrNetworkGrid.find(edgeContainer2[it->first][i]) == child2->mrNetworkGrid.end()){
				mrNeuron* closest;
				double dist = (maxDims[0]*maxDims[0]) + (maxDims[1]*maxDims[1]);
				for(NeuronGrid::iterator it2 = child2->mrNetworkGrid.begin(); it2 != child2->mrNetworkGrid.end(); ++it2){
					if(it != it2){
						if(dist > it->second->distanceTo(it2->second)){
							closest = it2->second;
							dist = it->second->distanceTo(it2->second);
						}
					}
				}
				if(!it->second->isConnection(closest->coords)){
					d = new delay((int)round(dist),"D");
					delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					s = new mrSynapse(weightContainer2[it->first][i],"W", this);
					d->addOutputElement((element*)s);
					d->addInputElement((element*)it->second);
					s->addInputElement((element*)d);
					s->addOutputElement((element*)closest);
					child2->delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					it->second->addOutputElement((element*)d,closest->coords);
					closest->addInputElement((element*)s,it->second->coords);
				}
			}
			else{
				NeuronGrid::iterator match = child2->mrNetworkGrid.find(edgeContainer2[it->first][i]);
				if(!it->second->isConnection(match->second->coords)){
					double dist = it->second->distanceTo(match->second);	
					d = new delay((int)round(dist),"D");
					delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					s = new mrSynapse(weightContainer2[it->first][i],"W", this);
					d->addOutputElement((element*)s);
					d->addInputElement((element*)it->second);
					s->addInputElement((element*)d);
					s->addOutputElement((element*)match->second);
					child2->delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					it->second->addOutputElement((element*)d,match->second->coords);
					match->second->addInputElement((element*)s,it->second->coords);
				}
			}
		}
	}
}


/*First pass at an affine(eg line, plane, hyperplane) crossover
 * Questions: How to choose the line?
 * Ideas: choose line between two points
 * pick random line
 * Do we do the same line through each network? probably for now
 * choose both from 1 of the parents
 */
//We need rand points == dimension?

/*
vector<mrNetwork*> mrNetwork::CrossoverAffine(mrNetwork* partner)
{
	int randVal = Random_32()%mrNetworkGrid.size();	
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	advance(randNeuron,randVal);
	int randVal2 = Random_32()%mrNetworkGrid.size();
	int c = 0;
	while(randVal2 == randVal){
		c++;
		if(c == 100){
			break;
		}
		randVal2 = Random_32()%mrNetworkGrid.size();
	}
	NeuronGrid::iterator randNeuron2 = mrNetworkGrid.begin();
	advance(randNeuron2,randVal2);

	if(c == 100){
		MRDANNA_ERROR("affine crossover- need to handle this case");
	}
	
		
}
*/

void mrNetwork::mutate()
{
	int mutation = Random_32()%7;
	/* going to experiment with this to hopefully get better results
	 * Apply mutations in proportion with network size
	 * consider Ceiling((NETSIZE - IO)/4) ?
	 */
	//int small = ceil((mrNetworkGrid.size() - nIn - nOut)/4.0);
	//int large = ceil(mrNetworkGrid.size()/4.0);

	switch(mutation){
		case 0 :
//			cout<<"***DOING MUTATE DELETE SYNAPSE***" <<endl;
			//Tested to 50 epochs
			mutationDeleteSynapse();
			break;
		case 1 :
//			cout<<"***DOING MUTATE ADD NEURON***" <<endl;
			//Tested to 50 epochs	
			mutationAddNeuron();
			break;
		case 2 :
//			cout<<"***DOING MUTATE ADD SYNAPSE***" <<endl;
			//Tested to 50 epochs
			mutationAddSynapse();
			break;
		case 3 :
//			cout<<"***DOING MUTATE DELETE NEURON***" <<endl;
			//Tested to 57 epochs
			mutationDeleteNeuron();
			break;
		case 4 :
//			cout<<"***DOING MUTATE MOVE NEURON***" <<endl;
			//Tested - ran to 48 epochs until process was killed
			mutationMoveNeuron();
			break;
		case 5 :
//			cout<<"***DOING MUTATE CHANGE THRESHOLD***" <<endl;
			//Test - ran to 20, killed to check for memleak
			mutationChangeThreshold();
			break;
		case 6 :
//			cout<<"***DOING MUTATE CHANGE WEIGHT***" <<endl;
//			//Tested - ran to 45 then killed
			mutationChangeWeight();
			break;
	}
}

//Call get random connection then delete that 
void mrNetwork::mutationDeleteSynapse()
{

	int randVal = Random_32()%mrNetworkGrid.size();
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	advance(randNeuron,randVal);
	vector<double> con = randNeuron->second->getRandomConnection();
	if(con[0] >= 0.0){
		NeuronGrid::iterator it = mrNetworkGrid.find(con);
		if(it == mrNetworkGrid.end()){
			MRDANNA_ERROR("Couldnt find known output connection");
			//cout<<con[0] <<" " <<con[1] <<endl;
			//int crash = 10/0;
		}
		else{
			deleteConnection(randNeuron->second,it->second);
		}
	}
}

//Changing this to add one incoming synapse and one outgoing synapse
void mrNetwork::mutationAddNeuron()
{
	mrNeuron* n = new mrNeuron(maxDims,maxThreshold,"N", this);
	if(mrNetworkGrid.find(n->coords) != mrNetworkGrid.end()){
		delete n;
		return;
	}
	/*
	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		double connect = Random_32() / (RAND_MAX + 1.);
		if(connectivity > connect){
			delay* d = new delay(n->distanceTo(it->second),"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));

			int weight = Random_32()%(weightMaxMag+1);
			if(Random_32()%2){
				weight = weight*-1;
			}
			mrSynapse* s = new mrSynapse(weight,"W");
			d->addOutputElement((element*)s);
			s->addInputElement((element*)d);
			if(Random_32()%2){
				n->addOutputElement((element*)d,it->second->coords);
				it->second->addInputElement((element*)s,n->coords);
				s->addOutputElement((element*)it->second);
			}
			else{
				it->second->addOutputElement((element*)d,n->coords);
				n->addInputElement((element*)s,it->second->coords);
				s->addOutputElement((element*)n);
			}
		}
	}
	*/
	int rand1 = Random_32()%mrNetworkGrid.size();
	int rand2 = Random_32()%mrNetworkGrid.size();
	NeuronGrid::iterator it, it2;
	it = mrNetworkGrid.begin();
	it2 = mrNetworkGrid.begin();
	advance(it,rand1);
	advance(it2,rand2);
	
	delay* d = new delay(n->distanceTo(it->second),"D");
	delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));

	int weight = Random_32()%(weightMaxMag+1);
	if(Random_32()%2){
		weight = weight*-1;
	}
	mrSynapse* s = new mrSynapse(weight,"W", this);
	d->addOutputElement((element*)s);
	s->addInputElement((element*)d);
	n->addInputElement((element*)s,it->second->coords);
	it->second->addOutputElement((element*)d,n->coords);
	s->addOutputElement((element*)n);

	d = new delay(n->distanceTo(it2->second),"D");
	delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));

	weight = Random_32()%(weightMaxMag+1);
	if(Random_32()%2){
		weight = weight*-1;
	}
	s = new mrSynapse(weight,"W", this);
	d->addOutputElement((element*)s);
	s->addInputElement((element*)d);
	n->addOutputElement((element*)d,it2->second->coords);
	it2->second->addInputElement((element*)s,n->coords);
	s->addOutputElement((element*)it2->second);


	mrNetworkGrid[n->coords] = n;	
}

void mrNetwork::mutationAddSynapse()
{
	int rand1 = Random_32()%mrNetworkGrid.size();
	int rand2 = Random_32()%mrNetworkGrid.size();
	if(rand1 == rand2){
		for(int i = 0; i < 100; i++){
			rand2 = Random_32()%mrNetworkGrid.size();
			if(rand1 != rand2){
				break;
			}
			else if(i == 99){
				return;
			}
		}
	}
	NeuronGrid::iterator it, it2;
	it = mrNetworkGrid.begin();
	it2 = mrNetworkGrid.begin();
	advance(it,rand1);
	advance(it2,rand2);
	if(it->second->isConnection(it2->second->coords)){
		return;
	}
	else{
		delay* d = new delay(it->second->distanceTo(it2->second),"D");
		delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
		int weight = (Random_32()%weightMaxMag) + 1;
		if(Random_32()%2){
			weight = weight*-1;
		}
		mrSynapse* s = new mrSynapse(weight,"W", this);
		d->addOutputElement((element*)s);
		d->addInputElement((element*)it->second);
		s->addInputElement((element*)d);
		s->addOutputElement((element*)it2->second);
		//cout<<"Adding Synapse through mutation" <<endl;
		it->second->addOutputElement((element*)d,it2->second->coords);
		it2->second->addInputElement((element*)s,it->second->coords);

	}

}

void mrNetwork::mutationDeleteNeuron()
{
	int randVal = Random_32()%mrNetworkGrid.size();
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	advance(randNeuron,randVal);
	if(randNeuron->second->getType() =="O" || randNeuron->second->getType() == "I"){
		for(int i = 0; i < 100; i++){
			randVal = Random_32()%mrNetworkGrid.size();
			randNeuron = mrNetworkGrid.begin();
			advance(randNeuron,randVal);
			if(randNeuron->second->getType() == "N"){
				break;
			}
			else if(i == 99){
				return;
			}
		}
	}
	deleteNeuron(randNeuron->second);

}

void mrNetwork::mutationMoveNeuron()
{
	int randVal = Random_32()%mrNetworkGrid.size();
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	advance(randNeuron,randVal);
	if(randNeuron->second->getType() =="O" || randNeuron->second->getType() == "I"){
		for(int i = 0; i < 100; i++){
			randVal = Random_32()%mrNetworkGrid.size();
			randNeuron = mrNetworkGrid.begin();
			advance(randNeuron,randVal);
			if(randNeuron->second->getType() == "N"){
				break;
			}
			else if(i == 99){
				return;
			}
		}
	}
	auto w = randNeuron->second->getOutgoingWeights();
	auto edgeO = randNeuron->second->getOutgoing();
	auto edgeI = randNeuron->second->getIncoming();
	auto w2 = randNeuron->second->getIncomingWeights();
	mrNeuron* n = new mrNeuron(randNeuron->second);
	n->network = this;
	/*What needs to happen - 
	 * update shuffle to be grid coords(comment out)
	 * check the result
	 */
	int full = 0;
	n->shuffle(maxDims);
	//Look at this loop closer if theres an issue
	while(mrNetworkGrid.find(n->coords) != mrNetworkGrid.end()){
		full++;
		if(full == 100){
			deleteNeuron(randNeuron->second);
			delete n;
			return;
		}
		n->shuffle(maxDims);
	}
	deleteNeuron(randNeuron->second);
	mrNetworkGrid.insert(pair<vector<double>, mrNeuron*>( n->coords,n));
	
	/*
	for(int i = 0; i < coords.size(); i++){
		coords[i] = randNeuron->second->coords[i];
	}
	*/

	NeuronGrid::iterator it;
	double dist;
	delay* d;
	mrSynapse* s;
	for(unsigned int i = 0; i < edgeO.size(); i++){
		it = mrNetworkGrid.find(edgeO[i]);
		if(it->first != edgeO[i]){
			cout<<"key-coord mismatch" <<endl;
			cout<<"going to: "; 
			for(unsigned int j = 0; j < it->first.size(); j++){
				cout<<it->first[j] <<" ";
			}
			cout<<endl;
			cout<<"me: " <<n->printCoords() <<endl;
		}
		dist = n->distanceTo(it->second);
		d = new delay((int)round(dist),"D");
		delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
		s = new mrSynapse(w[i],"W", this);
		d->addOutputElement((element*) s);
		s->addInputElement((element*) d);
		s->addOutputElement((element*)it->second);
		n->addOutputElement((element*)d,edgeO[i]);
		it->second->addInputElement((element*)s,n->coords);
	}
	/*
	for(NeuronGrid::iterator it2 = mrNetworkGrid.begin();it2 != mrNetworkGrid.end(); ++it2){
		if(it2->second->isConnection(coords)){
				it2->second->remove(coords);

				dist = it2->second->distanceTo(randNeuron->second);
				d = new delay((int)round(dist),"D");
				delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
				s = new mrSynapse(w2[i],"W");
				d->addOutputElement((element*) s);
				s->addInputElement((element*) d);
				randNeuron->second->addInputElement((element*)s,edgeI[i]);
				it->second->addOutputElement((element*)d,randNeuron->second->coords);
		}
	}
	*/
	
	for(unsigned int i = 0; i < edgeI.size(); i++){
		it = mrNetworkGrid.find(edgeI[i]);
		if(it == mrNetworkGrid.end()){
			cout<<"cant find known input neuron" <<edgeI[i][0] << " " <<edgeI[i][1] <<endl;
			
		}
		else{
			
			dist = it->second->distanceTo(n);
			d = new delay((int)round(dist),"D");
			delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
			s = new mrSynapse(w2[i],"W", this);
			d->addOutputElement((element*) s);
			s->addInputElement((element*) d);
			s->addOutputElement((element*)n);
			n->addInputElement((element*)s,edgeI[i]);
			it->second->addOutputElement((element*)d,n->coords);
		}
	}
	

}


void mrNetwork::mutationChangeThreshold()
{
	if(inCount == 0 || outCount == 0){
		MRDANNA_ERROR("0 in IO dims");
	}
	int numchange = mrNetworkGrid.size() - inCount - outCount;
	numchange = numchange / 4;
	set<int> s;

	int randVal;
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	for(int i = 0; i < numchange; i++){
		randVal = Random_32()%mrNetworkGrid.size();
		if(s.find(randVal) != s.end()){
			s.insert(randVal);
			advance(randNeuron,randVal);
			if(randNeuron->second->getType() == "N"){
				randNeuron->second->randThresh(maxThreshold);
			}
		}
	}



}

void mrNetwork::mutationChangeWeight()
{
	int randVal = Random_32()%mrNetworkGrid.size();
	NeuronGrid::iterator randNeuron = mrNetworkGrid.begin();
	advance(randNeuron,randVal);
	auto synapses = randNeuron->second->getSynapses();
	for(unsigned int i = 0; i < synapses.size(); i++){
		mrSynapse* s = (mrSynapse*)synapses[i];
		s->randWeight(weightMaxMag);
	}

}

/*
void mrNetwork::Random()
{
	inCount = 0;
	outCount = 0;
	clockcycle = 0;
	outputVector.resize(nOut);

	mrNeuron* n;
	mrSynapse* s;
	outputElement* o;
	delay* d;
	vector<double> cIn;
	vector<double> cOut;
	cIn.resize(embeddedDimension);
	cOut.resize(embeddedDimension);
	for(int i = 0; i < embeddedDimension; i++){
		cIn[i] = -1.0;
		cOut[i] = -2.0;
	}
	inputComponents.resize(nIn);
	for(int i = 0; i < nIn; i++){

		n = new mrNeuron(maxDims,maxThreshold, "I",inCount, this);
		s = new mrSynapse(n->getThreshold() + 1,"S"); 
		delay *d = new delay(0,"D");
		delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
		d->addOutputElement((element*) s);
		s->addInputElement((element*) d);
		s->addOutputElement((element*)n);
		n->addInputElement((element*)s, cIn);
		inCount++;
		inputComponents[i] = (element*)d;
		mrNetworkGrid[n->coords] = n;
	}
	for(int i = 0; i < nOut; i++){
		n = new mrNeuron(maxDims,maxThreshold,"O",outCount, this);
		o = new outputElement(i,&outputVector);
		outCount++;
		n->addOutputElement((element*)o,cOut);
		mrNetworkGrid[n->coords] = n;
	}
	default_random_engine generator(Random_32());;
	double stddev = .15*avgHidden;
	normal_distribution<double> distribution(avgHidden,stddev);
	int hiddenSize = (int)round(distribution(generator));
	hiddenSize = max(hiddenSize,0);
	for(int i = 0; i < hiddenSize; i++){
		n = new mrNeuron(maxDims, maxThreshold, "N", this);
		mrNetworkGrid[n->coords] = n;
	}
	double connect;
	int weight;

	for(NeuronGrid::iterator it = mrNetworkGrid.begin(); it != mrNetworkGrid.end(); ++it){
		for(NeuronGrid::iterator it2 = mrNetworkGrid.begin(); it2 != mrNetworkGrid.end(); ++it2){
			if(it->second != it2->second){
				connect = Random_32() / (RAND_MAX + 1.);
				if(connectivity > connect){
					d = new delay(it->second->distanceTo(it2->second),"D");
					delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
					weight = Random_32()%(weightMaxMag+1);
					if(Random_32()%2){
						weight = weight*-1;
					}
					s = new mrSynapse(weight,"W");
					d->addOutputElement((element*)s);
					d->addInputElement((element*)it->second);
					s->addInputElement((element*)d);
					s->addOutputElement((element*)it2->second);
					it->second->addOutputElement((element*)d,it2->second->coords);
					it2->second->addInputElement((element*)s,it->second->coords);
				}
			}
		}
	}

	//cout<<"Generated random net with size: " <<mrNetworkGrid.size() <<endl;
	//cout<<"Dimensions: " <<embeddedDimension <<endl;
}
*/


void mrNetwork::Random()
{
	inCount = 0;
	outCount = 0;
	clockcycle = 0;
	outputVector.resize(nOut);

	mrNeuron* n;
	mrSynapse* s;
	outputElement* o;
	//delay* d;
	vector<double> cIn;
	vector<double> cOut;
	cIn.resize(embeddedDimension);
	cOut.resize(embeddedDimension);
	for(int i = 0; i < embeddedDimension; i++){
		cIn[i] = -1.0;
		cOut[i] = -2.0;
	}
	inputComponents.resize(nIn);
	for(int i = 0; i < nIn; i++){

		n = new mrNeuron(maxDims,maxThreshold, "I",inCount, this);
		//n = new mrNeuron(maxDims,maxThreshold, "I",inCount,nIn);
		s = new mrSynapse(n->getThreshold() + 1,"S", this); 
		delay *d = new delay(0,"D");
		delays.insert(pair<uint64_t,element*>((uint64_t)d,(element*)d));
		d->addOutputElement((element*) s);
		s->addInputElement((element*) d);
		s->addOutputElement((element*)n);
		n->addInputElement((element*)s, cIn);
		inCount++;
		inputComponents[i] = (element*)d;
		mrNetworkGrid[n->coords] = n;
	}
	for(int i = 0; i < nOut; i++){
		n = new mrNeuron(maxDims,maxThreshold,"O",outCount, this);
		//n = new mrNeuron(maxDims,maxThreshold,"O",outCount,nOut);
		o = new outputElement(i,&outputVector);
		outCount++;
		n->addOutputElement((element*)o,cOut);
		mrNetworkGrid[n->coords] = n;
	}

	int hiddenSize = Random_32()%5 + 1;
	hiddenSize = max(hiddenSize,0);
	for(int i = 0; i < hiddenSize; i++){
		n = new mrNeuron(maxDims, maxThreshold, "N", this);	
		int full = 0;
		while(mrNetworkGrid.find(n->coords) != mrNetworkGrid.end()){
			delete n;
			if(full == 100){
				n = NULL;
			}
			n = new mrNeuron(maxDims, maxThreshold, "N", this);
			full++;
				
		}
		//if(n != NULL){
			mrNetworkGrid[n->coords] = n;
		//}
	}
	//double connect;
	//int weight;
	int numSynapses = Random_32()%10 + 1;
	//Using mutation - might cause trouble
	for(int i = 0; i < numSynapses; i++){
		mutationAddSynapse();
	}

}


void mrNetwork::Crossover(mrNetwork* partner, mrNetwork* c1, mrNetwork* c2, string crossoverType)
{
	if(crossoverType == "RC"){
		CrossoverRC(partner, c1, c2);
	}
	else{
		MRDANNA_ERROR("Invalid crossover type");
	}
}


#endif

#include <map>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include "mrNetwork.h"

using namespace std;

int main(int argc, char ** argv){

	//string filenum = argv[4];
	mrNetwork* mrNet = new mrNetwork; 
	mrNet->readNetworkFile(argv[1]);
	mrNet->pruneNetwork();
	cout<<mrNet->Serialize();

    return 0;
}

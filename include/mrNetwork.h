#ifndef mrNetwork_H
#define mrNetwork_H



//Think these are repeated
#include "tools.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <cstring>
#include <cstdint>
#include "mrNeuron.h"
#include "mrSynapse.h"
#include "delay.h"
#include "outputElement.h"
#include "utils/MOA.h"

using namespace std;

class mrNetwork{
	public:
		NeuronGrid mrNetworkGrid; // container of all the neurons, delay units, and their synapses
		int num_fired; // total number of times neurons fired
		mrNetwork(int nInputs,int nOutputs, int avgHidden, double connectivity,int e, vector<double> dims, int weightMaxMag, int maxThreshold);

		//char *net;
		char *input;
		//ofstream outputfile;

		mrNetwork(char *filename, char *inputFile, int Max);
		mrNetwork(string config, int nInputs, int nOutputs);
		mrNetwork(mrNetwork* m);
		
		mrNetwork(int inC, int outC, vector<double> &maxDims);
		mrNetwork(int inC, int outC, vector<double> &maxDims, double connect, int weightm, int maxt, int avgh);

		mrNetwork(int nInputs, int nOutputs, string config);
		mrNetwork();
		~mrNetwork();
//		void* makeInput(int i);

		/* 
		 * Going to have 2 modes of operation here
		 * the startNetwork is for reading from files
		 * going to add some apply functions for continuous operation
		 */
		void startNetwork();
		void ApplyInputs(int id, double val, double time);
		void ApplyInputs(vector<int> &ids, vector<double> &vals, vector<double> &times);
		
		void reset(); //zero out the delays and accumulators on the neurons so we can test new inputs

//		void printNetwork(ofstream &outputfile);
//		void writeOutput(ofstream &outputfile);
//		void printIrisfile(ofstream &outputfile);



		void readNetworkFile(const char* filename);
		void setInputFile(char* inputFile); 
		void setInputVector(string line, vector<bool>& inputs);
		/* 
		 * We might need one more setInputVector function 
		 * this would work with the apply inputs of the device
		 * that way the event queue gets added there, not here
		 * would require clockCycle management
		 * consider impact that might have in other places
		 */

		void Crossover(mrNetwork* p, mrNetwork* c1, mrNetwork* c2, string CrossoverType);
		void CrossoverRC(mrNetwork* p, mrNetwork* c1, mrNetwork* c2);


		string getHeader();
		string Serialize(char flag = 'I');
		void readSerialNetwork(string s);
		void Run(int cycles, vector<bool> &fireVector);
		vector<vector<int> > getOutputs();
		void Random();
		void mutate();
		void mutationDeleteSynapse();
		void mutationAddNeuron();
		void mutationAddSynapse();
		void mutationDeleteNeuron();
		void mutationMoveNeuron();
		void mutationChangeThreshold();
		void mutationChangeWeight();
		int inputSize();
		int outputSize();
		string config;
		string getConfig();
		void deleteNeuron(mrNeuron* n);
		void deleteConnection(mrNeuron* from, mrNeuron* to);
		void destructNeuron(mrNeuron* n);	
		void clearOldOutputs();

		int getTime();
		void setEOParams(mrNetwork* p);
		void Restore();
		string GetActivity();
		bool pruneNetwork();		
		void printNeuronStates();
		void setOutputSize();
		string printEvents();
		
		int IsInputIgnored(int inputid);
		int IsOutputIgnored(int outputid);

		uint32_t Random_32();
		double Random_Double();
		NeuroUtils::MOA **rng;
	private:
		ElementVector inputComponents;
		ElementVector outputComponents;
		ElementMap hiddenNeurons;
		int inCount;
		int outCount;
		vector<bool> outputVector;
		ElementMap networkContainer;
		ElementMap delays;
		int mapKey;
		int clockcycle;
		int clockcycleMax;
		vector<vector<int> > trackedOutputs;
		void trackOutputs();
		int embeddedDimension;
		vector<double> maxDims;

		/* App/EO params*/
		double connectivity;
		int nIn;
		int nOut;
		int avgHidden;

		/*Moving config parameters to network level
		 * idea - 2 modes, run from default/run from network string
		 * need to create list of variables
		 * need to update network readers/writers
		 * need to pass these values to functions
		 * instead of reading the defined value
		 * List of vars-
		 * maxThresh
		 * maxLevels
		 * maybe only allow fixed refrac
		 * learning params
		 * device/leakage when we get to those
		 */
		int weightMaxMag;
		int maxThreshold;
		int refracVal;
		double learningParam;
		string learningType;
		
		//Need an events data structure
		//map of vector of strings
		//each key is a clock cycle
		map<int,vector<string> > events;



};


#endif

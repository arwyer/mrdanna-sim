#ifndef mrNeuron_H
#define mrNeuron_H

#include "element.h"
//#include "coordinates.h"
#include "tools.h"
#include "mrNetwork.h"

//namespace

class mrNeuron : public element
{
	public:
		/* Constructors */
		//Leaving startcycle out of this for now - MIGHT be an issue
		mrNeuron(int r, string ty, int t,vector<double> &c, class mrNetwork* network);
		mrNeuron(vector<double> &c, string ty, class mrNetwork* network);
		mrNeuron(vector<double> &maxDims, int maxThresh, string ty, class mrNetwork* network);
		mrNeuron(vector<double> &maxDims, int maxThresh, string ty, int pos, class mrNetwork* network);
		mrNeuron(vector<double> &maxDims, int maxThresh, string ty, int pos, int numTot, class mrNetwork* network);
		mrNeuron(mrNeuron* copy);
		~mrNeuron();

		/* Create and run networks */
		void setRefractoryandThreshold(int r, int t);
		bool inRefPeriod();
		void apply(float w, int clockCycle,element* sender);
		void reset();
		void update(int clockCycle);
		void addInputElement(element* e, vector<double> &c);
		void addOutputElement(element* e, vector<double> &c);
		bool isConnection(vector<double> &c);
		void print(string &s,char flag = 'I');
		int getThreshold();
		element* getDelayUnit(vector<double> &c);
		vector<element*> getAllDelayUnits();
		void feedback(int f);

		/*EO*/
		void deleteRandomConnection();
		vector<double> getRandomConnection();

		/* Embedding */
		vector<double> coords;
		double distanceTo(mrNeuron* dest);
		vector< vector<double> > getOutgoing();
		vector< vector<double> > getIncoming();
		vector<int> getOutgoingWeights();
		vector<int> getIncomingWeights();
		void shuffle(vector<double> maxDims);
		void remove(vector<double> &v);
		void randThresh(int maxThreshold);
		vector<element*> getSynapses();
		int getOutputPos();
		int getInputPos();
		int IOid;
		string printCoords();
		string printCoords(const vector<double> &c);
		void dropIncoming(vector<double> &incN);
		void GetActivity(string& s);
		bool noIO();


		vector<float> collectAccums;
		vector<int> collectFireds;
		
		void reportEvents(int clockCycle, vector<string>& reported, vector<int>& reportTimes);
		int IOignored();
		class mrNetwork* network;
	private:
			
		NeuroIOMap inElements;
		NeuroIOMap outElements;
		bool inRefracPeriod;
		int refractoryPeriod;
		int threshold;
		float accumulator;
		bool fireNextCycle;
		int cycleLastFired;

		void depressSynapse(element* S, int val);
		void potentiateSynapse(element* S,int val);
		vector<vector<element*> > signals;
		int storedCycle; //Use this to get around order of neurons firing		
		int cycleToFire;

		/*Data structures we need for potentiation
		 *Store - synapses that push over the threshold
		 *		- these should all be on same CC, potentiate one at random?
		 *		- depress others?
		 *Any synapse that hits during refractory should be depressed
		 *Also need logic to drop all the synapses when we dip below threshold
		 *Also dont potentiae on input synapses
		 */	
		
		//Activity counters
		int noInput;
		int accumInput;
		int fired;
		float totalAccum;
		int totalCycles;
		bool gotInput;
		vector<string> events;
		vector<int> eventTimes;
};


#endif


#ifndef element_H
#define element_H

#include "tools.h"
#include <string>

using namespace std;
//namespace


//Comment these functions 
//right now print is returning strings, might take another look at this later

class element{
	public:
		virtual void apply(float w, int clockCycle, element* sender);
		virtual void reset();	
		virtual void update(int clockCycle);
		virtual void print(string &s,char flag = 'I');
		virtual void GetActivity(string &s);
		virtual ~element();
		virtual element* getDelayUnit();
		string getType();
		virtual int getId();
		int getConnectionWeight();
		int epos;
		element* findSynapse();
		void deleteConnections();
		virtual void feedback(int type,int val);	
	protected:
		int eWeight;
		string type;
		void printRecursive(string &s,int param, char flag = 'I');	
		ElementMap outputElements;
		ElementMap inputElements;
		float energyTotal;
		float activeEnergy;
		float passiveEnergy;
		int activeCycle;
		int totalCycle;
};


#endif

#ifndef mrSynapse_H
#define mrSynapse_H

//namespace
#include "element.h"
#include "tools.h"
#include "mrNetwork.h"

class mrSynapse : public element
{
	public:
		int currentClockCycle;
		int lastClockCycle;

		mrSynapse(int w, string t, class mrNetwork* network);
		void carryCharge(int clockcycle);
		void changeWeight(int clockcycle); // will determine to potentiate/depress/do nothing to a synapse
		void apply(float w, int clockCycle, element* sender);
		void update(int clockCycle);
		void reset();
		void addInputElement(element* e);
		void addOutputElement(element* e);
		void print(string &s,char flag); 
		int getWeight();
		void randWeight(int weightMaxMag);
		void feedback(int potOrDep, int val);
		void GetActivity(string &s);
		void potentiateSTDP(int val);
		void depressSTDP(int val);

	private:
		float weight;
		float weightConstant;
		int mapKey;
		float initWeight;	
		void potentiateConstant();
		void depressConstant();
		void potentiateReal();
		void depressReal();
		void setDeviceParams();

		//Activity
		int numLTP;
		int numLTD;
		int numActive;

		/*Putting params here for more accurate LTP/LTD*/
		//Right now we're assuming w +/- 1 for LTP/LTD
		float pdelR;
		float ndelR;
		float Rn; 
		float Rp;
		float HRS;
		float LRS;
		float Rmax;
		float Rmin;
		float Geff;
		float normG;
		float GeffMax;
		float rInitConst;
		int max;
		class mrNetwork* network;
};


#endif

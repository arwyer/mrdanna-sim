#ifndef mrparams_H
#define mrparams_H

/*This is the maximum magnitude of the synaptic weights
 *If the value is set to k, then the weights can range from -k to k
 *Also note that if Real LTP/LTD is used, then the model will still
 *be constrained by the same LRS -> HRS range
 *Should be an integer value
 */
#define MAXLEVEL 10

/*This is the maximum threshold value of the neuron
 *Notice the relationship between the weight and threshold
 *If a synapse has a weight of 1, and neuron has a threshold of 2
 *then it will take 2 synaptic fires to produce 1 neuron fire
 *Should be an integer value
 */
#define MAXTHRESHOLD 10

/*This defines the behavior of the threshold
 *If "VAR" then threshold varies from 1 to MAXTHRESHOLD
 *If "FIXED" threshold is fixed at MAXTHRESHOLD
 */
#define THRESHOLDTYPE "VAR"

/*This specifies the dimension that the network is embedded in
 *Currently, the model is only capable of 2D embeddings
 *Arbitrary embeddings are a consideration for future work
 *Should be an integer value
 */
#define DIMENSION 2

/*This specifies whether any learning should be done
 *If on, then learning based on LEARNINGTYPE will be used
 *Should be a string "ON" or "OFF"
 */
#define LEARNING "ON"

/*This specifies the learning type and controls LTP/LTD
 *"Constant" - increase/decrease weight by constant value, specified by
 *the LEARNINGPARAM variable
 *"Real" - perform learning that accurately models the device
 *"STDP" - not implemented yet
 *Should be a string specified above
 */

#define LEARNINGTYPE "Real" 

/*This is a value used by the the potentiation/depression functions
 *"Constant" - value to increase/decrease by
 *"Real" - unused, might make this the delR value
 *"STDP" - the integer number of cycles to track for STDP
 *Should be floating point value
 */
#define LEARNINGPARAM 3.0

/*This sets the maximum value for a neuron's refractory period
 *Should be an integer
 */
#define MAXREFRACPERIOD 1

/*This sets the minimum value for a neuron's refractory period
 *This is important for STDP
 *with STDP this value should be at least the learning param
 *if REFRACTYPE is FIXED this value is not used
 *not sure how this will work for 0
 *should be an integer
 */
#define MINREFRACPERIOD 1

/*This determines how the refractory periods should vary
 *"FIXED" - All neurons will have the same refractory period
 *          this refractory period will be MAXREFRACPERIOD
 *"VAR" - The refractory periods will be uniform random
 *        from 1 to MAXREFRACPERIOD
 *Should be a string
 */
#define REFRACTYPE "FIXED"

/*These are device parameters I'm adding for now
 *Might or might not be important later
 *I'll get feedback
 *probably need at least the geff/other synapse constants
 */
#define MEMHRS 50000
#define MEMLRS 5000
#define CLOCKPERIOD 40E-9
#define POSITIVESWITCHTIME 1E-6
#define NEGATIVESWITCHTIME 1E-6
#define POSITIVETHRESHOLDVOLTAGE .75
#define NEGATIVETHRESHOLDVOLTAGE .75
#define LEARNINGVOLTAGE 1.2

//possibly add geffmax, different from theoretical

#define THRESHOLDLIMIT 12


#endif

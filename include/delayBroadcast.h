#ifndef delayBroadcast_H
#define delayBroadcast_H

//includes and namespace

class delayBroadcast : public element
{
	public:
		delayBroadcast(int delay);
		void changeDelay(int newDelay);
		void reset();
		void update(int clockCycle);
		void apply(float w, int clockCycle,element* sender);
		void addInputElement(element* e);
		void addOutputElement(element* e);

	private:
		ElementMap inputElements;
		ElementMap outputElements;
		int ccDelay;
		int mapKey;
		std::uint64_t buf[2]; //128 bits of the shift register, broken into 2 64's
		std::uint64_t d1[2];
};

#include "delayBroadcast.hpp"

#endif


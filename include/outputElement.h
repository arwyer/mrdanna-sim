#ifndef outputElement_H
#define outputElement_H

#include "element.h"
#include "tools.h"

//need some includes, probably element

//class used to record output
//not necessary but its less hacky than looking at the output neurons 
//or giving them some special functionality
//add in the inheritance
class outputElement : public element
{
	public:
		void apply(float w, int clockCycle,element* sender);
		outputElement(int, vector<bool>*);
		void addInputElement(element* e);
		void print(string &s,char flag);
		void update(int clockCycle);
		void feedback(int f);
		void GetActivity(string &s);

	private:
		int position;
		vector<bool>* outputVector;	
};


#endif

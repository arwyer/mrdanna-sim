#ifndef mrDannaDevice_H
#define mrDannaDevice_H

#include "tools.h"
#include "neuro.h"

using namespace std;

class mrDannaEvent
{
	public:
		int clockCycle;
		int id;
		double weight;
		mrDannaEvent(int inputId, int inputClockCycle, double val);
	private:

};

class mrDannaDevice
{
	public:
		mrDannaDevice(const string &config,const string &device_specific_info);
		mrDannaDevice();
		~mrDannaDevice();
		
		void LoadNetwork(NeuroNetwork *network);
		void PullCurrentNetwork(NeuroNetwork* network);
		
		void ApplyInputs(int id, double val, double time);
		void ApplyInputs(vector<int> &ids, vector<double> &vals, vector<double> &times);
		void Run(double time);
		double GetTime();
		
		void SetUpOutput(int id, char type, double aftertime);
		double OutputLastFire(int id);
		int OutputCount(int id);
		void OutputVector(int id, vector<double> &times, vector<double> &vals);
		void Clear();
		void ClearActivity();
		void Restore();
		double getTime();


		NeuroNetwork* neuroNet;
		NeuroBatch* batch;
	private:
		string conf;
		int nInput;
		int nOutput;
		priority_queue<mrDannaEvent> eventQueue;	
		int clockCycle;
		int lastRun;
		vector<bool> fireVector;
		vector<char> outputType;
		vector<double> outputAfterTime;
};

#endif

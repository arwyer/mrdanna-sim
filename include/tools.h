#ifndef tools_H
#define tools_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <cstring>
#include <cstdint>
#include <random>
#include <set>
#include <queue>
#include <algorithm>
#include <ctime>
#include <iterator>
#include <csignal>
//#include "mrparams.h"
#include "utils/params.h"

typedef std::map <int, class mrSynapse *> SynapseContainer;
typedef std::map <uint64_t, class delay *> DelayContainer;
typedef std::vector<class element*> ElementVector;

//Print to cout and dont exit for now while we debug
inline void MRDANNA_ERROR(std::string msg)
{
	std::cout << msg <<std::endl;
}


//do class coordinates if not compile
typedef std::map < std::vector<double> , class mrNeuron *> NeuronGrid;
typedef std::map < std::vector<double> , class element*> NeuroIOMap; 
typedef std::map<int, class element*> ElementMap;

//Add a macro for connecting elements

#endif

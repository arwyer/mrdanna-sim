#ifndef delay_H
#define delay_H

//add namespace  
#include "element.h"
#include "tools.h"

class delay : public element{

	public:
		delay(int delay, string t);
		void delayChange(int i);
		void reset();
		void update(int clockCycle);
		void apply(float w, int clockCycle,element* sender);
		void addInputElement(element* e);
		void addOutputElement(element* e);
		void print(string &s, char flag);
		void feedback(int f);
		void GetActivity(string &s);

	private:
		int ccDelay; //delay
		int mapKey;
		std::uint64_t buf[2]; //128 bits of the shift register, broken into 2 64's
		std::uint64_t d1[2];
		int neuronID;
		vector<float> charges;
		unsigned int head;
		
		int chargesPassed;
};

#endif
